
Configuration Genesys IVR for MRCP
=====================================================

There are no unique configuration options specific to MRCP Server configuration to interoperate with Genesys IVR. Following table outlines recommended Genesys IVR settings for connecting to MRCP Server:

+---------------------------------+-------------------------------+
|OPTION                           |VALUE                          |
+=================================+===============================+
|vrm.client.resource.uri          |sip:smi@<MRCP server IP>:<port>|
+---------------------------------+-------------------------------+
|vrm.client.TransportProtocol     |MRCPv2                         |
+---------------------------------+-------------------------------+
|vrm.client.resource.name         |SPEECHMORPHING                 |
+---------------------------------+-------------------------------+
|vrm.client.resource.type         |TTS                            |
+---------------------------------+-------------------------------+
|vrm.client.SendLoggingTag        |true                           |
+---------------------------------+-------------------------------+
|vrm.client.ConnectPerSetup       |true                           |
+---------------------------------+-------------------------------+
|vrm.client.TTSInsertVoiceTag     |true                           |
+---------------------------------+-------------------------------+
|vrm.client.SpeechMarkerEncoding  |UTF-8                          |
+---------------------------------+-------------------------------+
|vrm.client.NoSpeechLanguageHeader|false                          |
+---------------------------------+-------------------------------+
|vrm.client.SendSessionXML        |false                          |
+---------------------------------+-------------------------------+



There is a set of documents for deployment/configuration/use of Genesys IVR platform (called Genesys Voice Platform – GVP).

Genesys Voice Platform documents: https://docs.genesys.com/Documentation/GVP

Options of the MRCPv2 client in GVP: https://docs.genesys.com/Documentation/Options/latest/GVP/MRCPv2_TTS-provision


Tag ```say-as``` in message
-----------------------------

You can use tag ```say-as``` to set settings in ```speak``` header. In ```say-as``` tag must be ```interpreted-as="smi_config"``` to set SMI voice settings. Support settings from API, list of settings name:
```age```, ```lang```, ```voice```, ```gender```, ```domain```, ```accent```, ```rate```, ```volume```, ```pitch```, ```style```, ```template```, ```template_original```