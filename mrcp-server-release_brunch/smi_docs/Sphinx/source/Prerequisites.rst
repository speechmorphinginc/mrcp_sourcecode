.. MRCP Server Documentation documentation master file, created by
   sphinx-quickstart on Tue Mar 13 11:43:57 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Prerequisites
=====================================================

For correct work MRCP Server need:

   #. Install packages: Docker, Docker-Compose, curl, git

   #. Operating system RHEL Linux, Ubuntu Linux, Debian Linux

   #. Server recommended configuration:

      * 8 CPUs

      * 32GB RAM

      * 2 x 512GB SSD

      * 2 x 1Gbps network cards