/*
 * Copyright 2008-2015 Arsen Chaloyan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Mandatory rules concerning plugin implementation.
 * 1. Each plugin MUST implement a plugin/engine creator function
 *    with the exact signature and name (the main entry point)
 *        MRCP_PLUGIN_DECLARE(mrcp_engine_t*) mrcp_plugin_create(apr_pool_t *pool)
 * 2. Each plugin MUST declare its version number
 *        MRCP_PLUGIN_VERSION_DECLARE
 * 3. One and only one response MUST be sent back to the received request.
 * 4. Methods (callbacks) of the MRCP engine channel MUST not block.
 *   (asynchronous response can be sent from the context of other thread)
 * 5. Methods (callbacks) of the MPF engine stream MUST not block.
 */

#include <libxml/parser.h>
#include <libxml/xmlIO.h>
#include <libxml/xinclude.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>


#include "mrcp_to_rest_engine.h"

/** Create demo synthesizer engine */
MRCP_PLUGIN_DECLARE(mrcp_engine_t*) mrcp_plugin_create(apr_pool_t *pool) {
    /* create demo engine */
    tts_synth_engine_t *tts_engine = apr_palloc(pool, sizeof (tts_synth_engine_t));
    apt_task_t *task;
    apt_task_vtable_t *vtable;
    apt_task_msg_pool_t *msg_pool;

    /* create task/thread to run demo engine in the context of this task */
    msg_pool = apt_task_msg_pool_create_dynamic(sizeof (tts_synth_msg_t), pool);
    tts_engine->task = apt_consumer_task_create(tts_engine, msg_pool, pool);
    if (!tts_engine->task) {
        return NULL;
    }
    task = apt_consumer_task_base_get(tts_engine->task);
    apt_task_name_set(task, SYNTH_ENGINE_TASK_NAME);
    vtable = apt_task_vtable_get(task);
    if (vtable) {
        vtable->process_msg = tts_synth_msg_process;
    }

    /* create engine base */
    return mrcp_engine_create(
            MRCP_SYNTHESIZER_RESOURCE, /* MRCP resource identifier */
            tts_engine, /* object to associate */
            &engine_vtable, /* virtual methods table of engine */
            pool); /* pool to allocate memory from */
}

/** Destroy synthesizer engine */
static apt_bool_t tts_synth_engine_destroy(mrcp_engine_t *engine) {
    tts_synth_engine_t *tts_engine = engine->obj;
    if (tts_engine->task) {
        apt_task_t *task = apt_consumer_task_base_get(tts_engine->task);
        apt_task_destroy(task);
        tts_engine->task = NULL;
    }
    return TRUE;
}

/** Open synthesizer engine */
static apt_bool_t tts_synth_engine_open(mrcp_engine_t *engine) {
    tts_synth_engine_t *tts_engine = engine->obj;
    if (tts_engine->task) {
        apt_task_t *task = apt_consumer_task_base_get(tts_engine->task);
        apt_task_start(task);
    }
    return mrcp_engine_open_respond(engine, TRUE);
}

/** Close synthesizer engine */
static apt_bool_t tts_synth_engine_close(mrcp_engine_t *engine) {
    tts_synth_engine_t *tts_engine = engine->obj;
    if (tts_engine->task) {
        apt_task_t *task = apt_consumer_task_base_get(tts_engine->task);
        apt_task_terminate(task, TRUE);
    }
    return mrcp_engine_close_respond(engine);
}

static int countActive = 0;
static int count_channels = 0;

/** Create demo synthesizer channel derived from engine channel base */
static mrcp_engine_channel_t* tts_synth_engine_channel_create(mrcp_engine_t *engine, apr_pool_t *pool) {
    mpf_termination_t *termination;

    countActive++;
    count_channels++;
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "countActive=%d", countActive);

    /* create demo synth channel */
    tts_synth_channel_t *synth_channel = apr_palloc(pool, sizeof (tts_synth_channel_t));
    bzero(synth_channel, sizeof(tts_synth_channel_t));


    synth_channel->tts_engine = engine->obj;
    synth_channel->speak_request = NULL;
    synth_channel->stop_response = NULL;
    synth_channel->time_to_complete = 0;
    synth_channel->completed = 0;
    synth_channel->paused = FALSE;
    synth_channel->audio_file = NULL;
    synth_channel->delta_sec = 0;
    synth_channel->jump_to_sec = 0;
    synth_channel->audio_position = 0;
    synth_channel->next_marker = NULL;
    synth_channel->current_num_audio_file_url = 0;
    synth_channel->is_show_start_read_log = 0;

    synth_channel->time_from_start = mtime();
    synth_channel->time_from_api_answer = 0;


    synth_channel->synth_voice_params = apr_palloc(pool, sizeof (struct smi_synth_voice_params));
    bzero(synth_channel->synth_voice_params, sizeof(struct smi_synth_voice_params));

    synth_channel->synth_voice_params->header_params = apr_palloc(pool, sizeof(struct mrcp_synth_header_t));
    bzero(synth_channel->synth_voice_params->header_params, sizeof(struct mrcp_synth_header_t));

    const char* pause_on_error = mrcp_engine_param_get(engine, "pause_on_error");
    synth_channel->pause_on_error = 1*1000;
    if(pause_on_error != NULL)
    {
        synth_channel->pause_on_error = atoi(pause_on_error);
    } 
    
    const char* loadingStep1Pause = mrcp_engine_param_get(engine, "pause_on_step_2_loading");
    synth_channel->loadingStep1Pause = 1*1000;
    if(loadingStep1Pause != NULL)
    {
        synth_channel->loadingStep1Pause = atoi(loadingStep1Pause);
    } 
    
    synth_channel->isGenerated = FALSE;
    synth_channel->currentLoadingStep = 1;
    synth_channel->currentPlayingStep = 1;
    bzero(synth_channel->files_array, MAX_FILES_IN_ANSWER * sizeof (FILE*));
    bzero(synth_channel->files_path_array, MAX_FILES_IN_ANSWER * sizeof (apt_str_t));
    bzero(synth_channel->marker_files_array, MAX_FILES_IN_ANSWER * sizeof (FILE*));
    bzero(synth_channel->marker_files_path_array, MAX_FILES_IN_ANSWER * sizeof (apt_str_t));
    bzero(synth_channel->audio_file_url, MAX_FILES_IN_ANSWER * sizeof (apt_str_t));
    bzero(synth_channel->request_id, 4096 * sizeof(char));

    synth_channel->load_file_thread = 0;

    synth_channel->synth_voice_params->smi_template_original = 3;
    synth_channel->synth_voice_params->smi_template_id = 0;
    synth_channel->synth_voice_params->speach_accent.buf = NULL;
    synth_channel->synth_voice_params->speach_accent.length = 0;
    synth_channel->logging_tag.buf = NULL;
    synth_channel->logging_tag.length = 0;
    synth_channel->synth_voice_params->smi_style_id = 0;
    synth_channel->synth_voice_params->audio_params.pitch = 0;
    synth_channel->synth_voice_params->audio_params.rate = 0;
    synth_channel->synth_voice_params->audio_params.volume = 0;
    synth_channel->synth_voice_params->language_params.language_name.buf = NULL;
    synth_channel->synth_voice_params->language_params.language_name.length = 0;
    synth_channel->synth_voice_params->language_params.language_id = 0;
    synth_channel->synth_voice_params->language_params.supported = 1;
    synth_channel->is_final = 0;
    synth_channel->is_canceled = FALSE;
    synth_channel->is_stoped_audio = FALSE;

    synth_channel->synth_voice_params->smi_domain_id = 0;
    synth_channel->synth_voice_params->header_params->voice_param.age = 0;
    synth_channel->synth_voice_params->header_params->voice_param.gender = VOICE_GENDER_NEUTRAL;

    const char* gender = mrcp_engine_param_get(engine, "voice_param_gender");
    apt_log(SYNTH_LOG_MARK, APT_PRIO_DEBUG, "voice_param_gender=%s",   gender);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_DEBUG, "voice_param_age=%s",      mrcp_engine_param_get(engine, "voice_param_age"));
    apt_log(SYNTH_LOG_MARK, APT_PRIO_DEBUG, "voice_param_variant=%s",  mrcp_engine_param_get(engine, "voice_param_variant"));
    apt_log(SYNTH_LOG_MARK, APT_PRIO_DEBUG, "speech_language=%s",      mrcp_engine_param_get(engine, "speech_language"));
    apt_log(SYNTH_LOG_MARK, APT_PRIO_DEBUG, "speaker_profile=%s",      mrcp_engine_param_get(engine, "speaker_profile"));
    apt_log(SYNTH_LOG_MARK, APT_PRIO_DEBUG, "request_timeout=%s",      mrcp_engine_param_get(engine, "request_timeout"));
    apt_log(SYNTH_LOG_MARK, APT_PRIO_DEBUG, "request_tries=%s",        mrcp_engine_param_get(engine, "request_tries"));


    if (gender == NULL) {
        synth_channel->synth_voice_params->header_params->voice_param.gender = VOICE_GENDER_NEUTRAL;
    } else if (strcasecmp(gender, "male") == 0) {
        synth_channel->synth_voice_params->header_params->voice_param.gender = VOICE_GENDER_MALE;
    } else if (strcasecmp(gender, "female") == 0) {
        synth_channel->synth_voice_params->header_params->voice_param.gender = VOICE_GENDER_FEMALE;
    } else if (strcasecmp(gender, "neutral") == 0 || strcasecmp(gender, "all") == 0) {
        synth_channel->synth_voice_params->header_params->voice_param.gender = VOICE_GENDER_NEUTRAL;
    }

    mpf_stream_capabilities_t *capabilities;
    capabilities = mpf_source_stream_capabilities_create(pool);


    const char* codec = mrcp_engine_param_get(engine, "codec");
    if (codec == NULL || strcasecmp(codec, "PCMU") == 0)
    {
        mpf_codec_capabilities_add(&capabilities->codecs, MPF_SAMPLE_RATE_8000, "PCMU");
        mpf_codec_capabilities_add(&capabilities->codecs, MPF_SAMPLE_RATE_8000, "PCMA");
    }
    else
    {
        mpf_codec_capabilities_add(&capabilities->codecs, MPF_SAMPLE_RATE_8000, "PCMA");
        mpf_codec_capabilities_add(&capabilities->codecs, MPF_SAMPLE_RATE_8000, "PCMU");
    }

    /* create media termination */
    termination = mrcp_engine_audio_termination_create(synth_channel, /* object to associate */
            &audio_stream_vtable, /* virtual methods table of audio stream */
            capabilities, /* stream capabilities */
            pool); /* pool to allocate memory from */

    /* create engine channel base */
    synth_channel->channel = mrcp_engine_channel_create(engine, /* engine */
            &channel_vtable, /* virtual methods table of engine channel */
            synth_channel, /* object to associate */
            termination, /* associated media termination */
            pool); /* pool to allocate memory from */

    return synth_channel->channel;
}

/**
 * Destroy engine channel
 **/
static apt_bool_t tts_synth_channel_destroy(mrcp_engine_channel_t *channel) {
    /* nothing to destroy */
    tts_synth_channel_t *synth_channel = channel->method_obj;
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m tts_synth_channel_destroy", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
    return TRUE;
}

/** Open engine channel (asynchronous response MUST be sent)*/
static apt_bool_t tts_synth_channel_open(mrcp_engine_channel_t *channel) {
    return tts_synth_msg_signal(DEMO_SYNTH_MSG_OPEN_CHANNEL, channel, NULL);
}

/**
 *  Close and delete all sound files
 *  Close engine channel (asynchronous response sent)*/
static apt_bool_t tts_synth_channel_close(mrcp_engine_channel_t *channel) {

    countActive--;
    tts_synth_channel_t *synth_channel = channel->method_obj;
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m countActive=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, countActive);

    /*Doesnt work thread close with pointer synth_channel->load_file_thread*/
    synth_channel->is_canceled = TRUE;
    synth_channel->is_stoped_audio = TRUE;

    if(synth_channel->load_file_thread != 0)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m start pthread_join", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        pthread_join(synth_channel->load_file_thread, NULL);
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m end pthread_join", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
    }

    if(synth_channel->audio_file_url[1].length <= 0 || synth_channel->audio_file_url[1].buf == NULL )
    {
        apt_bool_t res = tts_synth_msg_signal(DEMO_SYNTH_MSG_CLOSE_CHANNEL, channel, NULL);
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_close(%d)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, res);
        return res;
    }

    int i;
    for (i = 1; i <= synth_channel->current_num_audio_file_url; i++)
    {
        if(synth_channel->audio_file_url[i].buf == NULL || !synth_channel->audio_file_url[i].length)
        {
            continue;
        }

        int urllen = strlen(synth_channel->audio_file_url[i].buf) - strlen(".1.pcmu");
        const char* fileNumber = synth_channel->audio_file_url[i].buf + urllen;
        int j;
        int fileNumber_len = 0;
        for(j = urllen - 1; j> 0; j--)
        {
            if(synth_channel->audio_file_url[i].buf[j] == '_')
            {
                break;
            }
            fileNumber--;
            fileNumber_len++;
        }

        json_object* post_json_object = json_object_new_object();
        json_object_object_add(post_json_object, "filenumber", json_object_new_string_len(fileNumber, fileNumber_len));

        // Call stop
        // http://hostname:portnumber/smorphing/1.0/stop
        json_object* res = api_request(channel, post_json_object, "/stop");
        if(res != NULL)
        {
            json_object_put(res);
        }
        json_object_put(post_json_object);
    }



    for (i = 1; i < MAX_FILES_IN_ANSWER; i++)
    {
        if (synth_channel->files_array[i] > 0) {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m close files audio fclose[%d]", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, i);
            fclose(synth_channel->files_array[i]);
        }

        if (synth_channel->marker_files_array[i] > 0) {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m close files marker fclose[%d]", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, i);
            fclose(synth_channel->marker_files_array[i]);
        }

        if (synth_channel->files_array[i] <= 0) {
            break;
        }
    }

    if(synth_channel->isSMB)
    {
        apt_bool_t res = tts_synth_msg_signal(DEMO_SYNTH_MSG_CLOSE_CHANNEL, channel, NULL);
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_close(%d)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, res);
        return res;
    }

    for (i = 1; i < MAX_FILES_IN_ANSWER; i++)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m remove files[%d]:%s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, i, synth_channel->files_path_array[i].buf);
        if (synth_channel->files_path_array[i].buf != NULL) {
            remove(synth_channel->files_path_array[i].buf);
        }

        if (synth_channel->marker_files_path_array[i].buf != NULL) {
            remove(synth_channel->marker_files_path_array[i].buf);
        }

        if (synth_channel->files_array[i] <= 0) {
            break;
        }
    }

    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m complete", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);

    apt_bool_t res = tts_synth_msg_signal(DEMO_SYNTH_MSG_CLOSE_CHANNEL, channel, NULL);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_close(%d)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, res);
    return TRUE;
}

/** Process MRCP channel request (asynchronous response MUST be sent)*/
static apt_bool_t tts_synth_channel_request_process(mrcp_engine_channel_t *channel, mrcp_message_t *request)
{
    return tts_synth_msg_signal(DEMO_SYNTH_MSG_REQUEST_PROCESS, channel,request);
}

/*
 * determine which SMI age_id corresponds to this age. The possible age_id is specified in the config file.
 *
 */
int64_t get_smi_age_id(loadFileCall* arg, apr_size_t age) {

    if(age == 0)
    {
        return 0;
    }

    const char *start_age = "age7";
    char *age_name = apr_palloc(arg->channel->pool, (strlen(start_age) + 1)*sizeof(char));
    strcpy(age_name, start_age);
    int i;
    for (i = 7; i > 0; i--, age_name[3]--) {
        int next_age = atoi(mrcp_engine_param_get(arg->channel->engine, age_name));
        if (age >= next_age) {
            return i;
        }
    }
}

int64_t get_smi_language_id(loadFileCall* arg){


    //return atoll(mrcp_engine_param_get(arg->channel->engine, "smi_language_id"));

    tts_synth_channel_t *synth_channel = arg->channel->method_obj;
    struct smi_synth_voice_params *voice_params = synth_channel->synth_voice_params;

    if (voice_params->language_params.language_id != 0)
    {
        return voice_params->language_params.language_id;
    }

    json_object* post_json_object = json_object_new_object();
    //json_object_object_add(post_json_object, "language_name", json_object_new_string(voice_params->language_params.language_name.buf));

    json_object_object_add(post_json_object, "send_unsupported", json_object_new_boolean(FALSE));

    json_object* result_json_object = api_request(arg->channel, post_json_object, "/language");
    json_object_put(post_json_object);

    if (result_json_object == NULL){
        apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Invalid response json languages is null", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        return 0;
    }

    json_object* results = json_object_object_get(result_json_object, "results");

    if (results == NULL || json_object_get_type(results) != json_type_array || json_object_array_length(results) <= 0)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Invalid response json languages array", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        json_object_put(result_json_object);
        return 0;
    }

    // language id by default
    const char* default_speech_language = mrcp_engine_param_get(arg->channel->engine, "speech_language");
    const char* smi_language_id = NULL;

    json_object* langs = results;
    int langs_len = json_object_array_length(results);

    int i;
    for(i = 0; i < langs_len; i++)
    {
        json_object* curent_lang = json_object_array_get_idx(langs, i);

        if (curent_lang == NULL || json_object_get_type(curent_lang) != json_type_object)
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Invalid response json language object", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
            continue;
        }

        json_object* curent_lang_name = json_object_object_get(curent_lang, "name");
        if(curent_lang_name == NULL || json_object_get_type(curent_lang_name) != json_type_string)
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Invalid response json language name string", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
            continue;
        }

        json_object* curent_lang_language_id = json_object_object_get(curent_lang, "language_id");
        if(curent_lang_name == NULL
                                    ||
                                    (
                                           json_object_get_type(curent_lang_language_id) != json_type_string
                                        && json_object_get_type(curent_lang_language_id) != json_type_int
                                        && json_object_get_type(curent_lang_language_id) != json_type_double
                                    )
            )
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Invalid response json language language_id value", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
            continue;
        }

        const char* lang_name = json_object_get_string(curent_lang_name);
        if(voice_params->language_params.language_name.buf != NULL && voice_params->language_params.language_name.length != 0
            && strcasecmp(lang_name, voice_params->language_params.language_name.buf) == 0)
        {
            smi_language_id = json_object_get_string(curent_lang_language_id);
            break;
        }
        else if(strcasecmp(lang_name, default_speech_language) == 0)
        {
            smi_language_id = json_object_get_string(curent_lang_language_id);
        }
    }

    if (smi_language_id == NULL) {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING,
                "\x1b[1;30msession=%s logging-tag=%s\x1b[0m get_smi_language_id with name `%s` or `%s` not found",
                synth_channel->channel_id.buf, synth_channel->logging_tag.buf, voice_params->language_params.language_name.buf, default_speech_language);
        json_object_put(result_json_object);
        return 0;
    }

    // return the final value
    int64_t result = atoll(smi_language_id);
    json_object_put(result_json_object);

    return result;
}

/**
 * With the filter according to the parameters:
 * long language_id
 * long age_id
 * String gender_id
 * String voicename
 *
 * Having received the id of the voice, I send the smi_voice_id parameter in the synthesis query.
 */
int64_t get_smi_voice_id(loadFileCall* arg) {

    // get the MRCP data
    tts_synth_channel_t *synth_channel = arg->channel->method_obj;
    smi_synth_voice_params *voice_params = synth_channel->synth_voice_params;

    int gender = VOICE_GENDER_COUNT -1 -voice_params->header_params->voice_param.gender;
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m voice_params->name: %s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, voice_params->voice_params.voice_name.buf);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m voice_params->age: %d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, voice_params->header_params->voice_param.age);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m voice_params->gender: %d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, gender);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m voice_params->language_name: %s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, voice_params->language_params.language_name.buf);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m voice_params->language_id: %d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, voice_params->language_params.language_id);

    // we shove them all into JSON according to the SMI API
    json_object* post_json_object = json_object_new_object();

    if(voice_params->voice_params.voice_name.buf == NULL
            && !voice_params->header_params->voice_param.age
            && !gender
            && !voice_params->language_params.language_id)
    {
        const char* smi_voice_id = mrcp_engine_param_get(arg->channel->engine, "smi_voice_id");
        int64_t result = atoll(smi_voice_id);

        if(result)
        {
            json_object_put(post_json_object);
            return result;
        }
        else
        {
            // smi_voice_id is missing and refer to the configuration voice_param_name
            const char* voice_param_name = mrcp_engine_param_get(arg->channel->engine, "voice_param_name");
            if(voice_param_name != NULL && strlen(voice_param_name) > 0)
            {
                // write voice param_name for the request to api
                json_object_object_add(post_json_object, "voicename", json_object_new_string(voice_param_name));
            }
        }
    }


    json_object_object_add(post_json_object, "age_id", json_object_new_int64(get_smi_age_id(arg, voice_params->header_params->voice_param.age)));

    json_object_object_add(post_json_object, "gender_id", json_object_new_int64(gender));

    // Checking the voice_name from the query
    if (voice_params->voice_params.voice_name.buf != NULL && voice_params->voice_params.voice_name.length > 0)
    {
        // only if we specified name
        json_object_object_add(post_json_object, "voicename", json_object_new_string(apt_string_buffer_get(&voice_params->voice_params.voice_name)));
    }


    json_object_object_add(post_json_object, "language_id", json_object_new_int64(get_smi_language_id(arg)));

    json_object_object_add(post_json_object, "send_unsupported", json_object_new_boolean(FALSE));

    // we get the answer and check if there are suitable voices
    json_object* result_json_object = api_request(arg->channel, post_json_object, "/voices");
    json_object_put(post_json_object);

    if (result_json_object == NULL){
        apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Invalid response json voices is empty", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        return 0;
    }

    // voice id by default
    const char* smi_voice_id = NULL;

    json_object* results = json_object_object_get(result_json_object, "results");

    if (results == NULL || json_object_get_type(results) != json_type_array || json_object_array_length(results) <= 0)
    {
        json_object_put(result_json_object);
        apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Invalid response json voices results is empty or is not array", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        return 0;
    }

    json_object* first_result = json_object_array_get_idx(results, 0);

    if(first_result == NULL || json_object_get_type(first_result) != json_type_object)
    {
        json_object_put(result_json_object);
        apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Invalid response json voices first_result is null or is not object", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        return 0;
    }

    json_object* json_smi_voice_id = json_object_object_get(first_result, "smi_voice_id");

    if(json_smi_voice_id == NULL)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Invalid response json voices first_result smi_voice_id is null", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        json_object_put(result_json_object);
        return 0;
    }

    smi_voice_id = json_object_get_string(json_smi_voice_id);

    if (smi_voice_id == NULL)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Invalid response json voices first_result smi_voice_id string is null", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        json_object_put(result_json_object);
        return 0;
    }

    // return the final value
    int64_t result = atoll(smi_voice_id);
    json_object_put(result_json_object);

    return result;
}

int64_t get_smi_domain_id(loadFileCall* arg)
{
    tts_synth_channel_t *synth_channel = arg->channel->method_obj;
    if(synth_channel->synth_voice_params->smi_domain_id != 0)
    {
        return synth_channel->synth_voice_params->smi_domain_id;
    }

    const char* smi_domain_id = mrcp_engine_param_get(arg->channel->engine, "smi_domain_id");
    if (smi_domain_id == NULL) {
        return 0;
    }

    return atoll(smi_domain_id);
}

/** String table of MRCP synthesizer header fields (mrcp_synthesizer_header_id) */
apt_str_t synth_header_string_table[] = {
	{"Jump-Size",            9},
	{"Kill-On-Barge-In",    16},
	{"Speaker-Profile",     15},
	{"Completion-Cause",    16},
	{"Completion-Reason",   17},
	{"Voice-Gender",        12},
	{"Voice-Age",            9},
	{"Voice-Variant",       13},
	{"Voice-Name",          10},
	{"Prosody-Volume",      14},
	{"Prosody-Rate",        12},
	{"Speech-Marker",       13},
	{"Speech-Language",     15},
	{"Fetch-Hint",          10},
	{"Audio-Fetch-Hint",    16},
	{"Failed-Uri",          10},
	{"Failed-Uri_Cause",    16},
	{"Speak-Restart",       13},
	{"Speak-Length",        12},
	{"Load-Lexicon",        12},
	{"Lexicon-Search-Order",20},
        {"Logging-Tag",         11}
};

/** String table of MRCP voice-gender fields (mrcp_voice_gender_t) */
apt_str_t voice_gender_string_table[] = {
	{"male",   4},
	{"female", 6},
	{"neutral",7}
};

void tts_synth_channel_set_params_validator(mrcp_engine_channel_t *channel, mrcp_message_t *request, mrcp_message_t *response)
{
    apt_header_field_t *list_element_to_head = request->header.header_section.ring.prev;
    apt_header_field_t *list_element_to_tail = request->header.header_section.ring.prev;
    int step = 0;
    int is_find;
    int is_valid = 0;
    int j, i;
    response->start_line.status_code = MRCP_STATUS_CODE_UNSUPPORTED_PARAM;
    //header validation
    do {
        //operation for validate header
        int count = sizeof(synth_header_string_table)/ sizeof(apt_str_t);

        for (i = 0; i < count; i++)
        {
            is_find = strcasecmp(synth_header_string_table[i].buf, list_element_to_tail->name.buf);
            if (is_find == 0)
            {
                //validation voice_age value
                if (strcasecmp(list_element_to_tail->name.buf, "Voice-Age") == 0)
                {
                    if(atoi(list_element_to_tail->value.buf) >= 0)
                    {
                        response->start_line.status_code = MRCP_STATUS_CODE_SUCCESS_WITH_IGNORE ;
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
                //validation gender value
                if (strcasecmp(list_element_to_tail->name.buf, "Voice-Gender") == 0)
                {
                    for(j = 0; j < 3; j++)
                    {
                        if (strcasecmp(list_element_to_tail->value.buf, voice_gender_string_table[j].buf) == 0)
                        {
                            response->start_line.status_code = MRCP_STATUS_CODE_SUCCESS_WITH_IGNORE ;
                            return;
                        }
                    }
                    return;
                }
                response->start_line.status_code = MRCP_STATUS_CODE_SUCCESS_WITH_IGNORE ;
                return;
            }
            is_find = strcasecmp(synth_header_string_table[i].buf, list_element_to_head->name.buf);
            if (is_find == 0)
            {
                //validation voice_age value
                if (strcasecmp(list_element_to_head->name.buf, "Voice-Age") == 0)
                {
                    if(atoi(list_element_to_head->value.buf) >= 0)
                    {
                        response->start_line.status_code = MRCP_STATUS_CODE_SUCCESS_WITH_IGNORE ;
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
                //validation gender value
                if (strcasecmp(list_element_to_head->name.buf, "Voice-Gender"))
                {
                    for(j = 0; j < 3; j++)
                    {
                        if (strcasecmp(list_element_to_head->value.buf, voice_gender_string_table[j].buf) == 0)
                        {
                            response->start_line.status_code = MRCP_STATUS_CODE_SUCCESS_WITH_IGNORE ;
                            return;
                        }
                    }
                    return;
                }
                response->start_line.status_code = MRCP_STATUS_CODE_SUCCESS_WITH_IGNORE ;
                return;
            }
        }
        *list_element_to_tail = *list_element_to_tail->link.next;
        *list_element_to_head = *list_element_to_head->link.prev;
        step++;
    }while(step < 50);
}

void tts_synth_channel_put_params(mrcp_message_t *request, mrcp_engine_channel_t *channel)
{
    tts_synth_channel_t *synth_channel = channel->method_obj;

    mrcp_synth_header_t *req_synth_header;
    /* get synthesizer header */
    req_synth_header = mrcp_resource_header_get(request);
    //apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "INSIDE FUNCTION PUT PARAMS");
    if (req_synth_header)
    {
        /* check voice age header */
        if (mrcp_resource_header_property_check(request, SYNTHESIZER_HEADER_VOICE_AGE) == TRUE)
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "Set Voice Age [%"APR_SIZE_T_FMT"]", req_synth_header->voice_param.age);
            synth_channel->synth_voice_params->header_params->voice_param.age = req_synth_header->voice_param.age;
        }

        /* check voice name header */
        if (mrcp_resource_header_property_check(request, SYNTHESIZER_HEADER_VOICE_NAME) == TRUE)
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "Set Voice Name [%s]", req_synth_header->voice_param.name.buf);
            apt_string_copy(&synth_channel->synth_voice_params->voice_params.voice_name, &req_synth_header->voice_param.name, channel->pool);
        }

        if (mrcp_resource_header_property_check(request, SYNTHESIZER_HEADER_VOICE_GENDER) == TRUE)
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "Set Voice GENDER [%d]", req_synth_header->voice_param.gender);
            synth_channel->synth_voice_params->header_params->voice_param.gender = req_synth_header->voice_param.gender;
        }

        /*if (mrcp_resource_header_property_check(request, SYNTHESIZER_HEADER_VOICE_VARIANT) == TRUE)
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "Set Voice variant [%d]", req_synth_header->voice_param.variant);
            synth_channel->synth_voice_params->header_params->voice_param.variant = req_synth_header->voice_param.variant;
        }*/

        if (mrcp_resource_header_property_check(request, SYNTHESIZER_HEADER_SPEECH_LANGUAGE) == TRUE)
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "Set Voice LANGUAGE [%s]", req_synth_header->speech_language.buf);
            apt_string_copy(&synth_channel->synth_voice_params->language_params.language_name, &req_synth_header->speech_language, channel->pool);
        }

        /*if (mrcp_resource_header_property_check(request, SYNTHESIZER_HEADER_SPEAKER_PROFILE) == TRUE)
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "Set Voice speaker_profile [%s]", req_synth_header->speaker_profile.buf);
            apt_string_copy(&synth_channel->synth_voice_params->header_params->speaker_profile, &req_synth_header->speaker_profile, channel->pool);
        }*/
    }
}

xmlNodePtr findXMLNodeByName(xmlDocPtr doc, xmlNodePtr cur, const char* node_name)
{
    cur = cur->xmlChildrenNode;
    while (cur != NULL)
    {
        if ((!xmlStrcmp(cur->name, (const xmlChar *)node_name)))
        {
            return cur;
        }
        else
        {
            xmlNodePtr res = findXMLNodeByName(doc, cur, node_name);
            if(res != NULL)
            {
                return res;
            }
        }
        cur = cur->next;
    }

    return NULL;
}

/**
 *  Example parser https://github.com/libexpat/libexpat/blob/master/expat/examples/elements.c
 */
void parser_add_property(loadFileCall* arg, const char* key, const char* value)
{
    tts_synth_channel_t *synth_channel = arg->channel->method_obj;
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m set param %s=%s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, key, value);

    if(strcasecmp(key, "age") == 0 || strcasecmp(key, "xml:age") == 0)
    {
        synth_channel->synth_voice_params->header_params->voice_param.age = atoi(value);
    }
    else if(strcasecmp(key, "lang") == 0 || strcasecmp(key, "xml:lang") == 0)
    {
        int j, k = 0;
        for (j = 0; j < strlen(value); j++)
        {
            if (value[j] < '0' || value[j] > '9'){
                break;
            }
            k++;
        }
        if (k == strlen(value))
        {
            synth_channel->synth_voice_params->language_params.language_id = atoll(value);
        }
        else
        {
            apt_string_assign(&synth_channel->synth_voice_params->language_params.language_name, value, arg->channel->pool);
        }
    }
    else if(strcasecmp(key, "voice") == 0 || strcasecmp(key, "xml:voice") == 0)
    {
        int j, k = 0;
        for (j = 0; j < strlen(value); j++)
        {
            if (value[j] < '0' || value[j] > '9'){
                break;
            }
            k++;
        }
        if (k == strlen(value))
        {
            synth_channel->synth_voice_params->voice_params.voice_id = atoll(value);
        }
        else
        {
            apt_string_assign(&synth_channel->synth_voice_params->voice_params.voice_name, value, arg->channel->pool);
        }
    }
    else if(strcasecmp(key, "gender") == 0 || strcasecmp(key, "xml:gender") == 0)
    {
        if (strcasecmp(value, "Male") == 0)
        {
            synth_channel->synth_voice_params->header_params->voice_param.gender = 2;
        }
        else if (strcasecmp(value, "Female") == 0)
        {
            synth_channel->synth_voice_params->header_params->voice_param.gender = 1;
        }
        else if (strcasecmp(value, "All") == 0)
        {
            synth_channel->synth_voice_params->header_params->voice_param.gender = 0;
        }
        else
        {
            synth_channel->synth_voice_params->header_params->voice_param.gender = atoi(value);
        }
    }
    else if(strcasecmp(key, "domain") == 0 || strcasecmp(key, "xml:domain") == 0)
    {
        synth_channel->synth_voice_params->smi_domain_id = atoll(value);
    }
    else if(strcasecmp(key, "accent") == 0 || strcasecmp(key, "xml:accent") == 0)
    {
        apt_string_assign(&synth_channel->synth_voice_params->speach_accent, value, arg->channel->pool);
    }
    else if(strcasecmp(key, "rate") == 0 || strcasecmp(key, "xml:rate") == 0)
    {
        synth_channel->synth_voice_params->audio_params.rate = atoi(value);
    }
    else if(strcasecmp(key, "volume") == 0 || strcasecmp(key, "xml:volume") == 0)
    {
        synth_channel->synth_voice_params->audio_params.volume = atoi(value);
    }
    else if(strcasecmp(key, "pitch") == 0 || strcasecmp(key, "xml:pitch") == 0)
    {
        synth_channel->synth_voice_params->audio_params.pitch = atoi(value);
    }
    else if(strcasecmp(key, "style") == 0 || strcasecmp(key, "xml:style") == 0)
    {
        synth_channel->synth_voice_params->smi_style_id = atoll(value);
    }
    else if(strcasecmp(key, "template") == 0 || strcasecmp(key, "xml:template") == 0)
    {
        synth_channel->synth_voice_params->smi_template_id = atoll(value);
    }
    else if(strcasecmp(key, "template_original") == 0 || strcasecmp(key, "xml:template_original") == 0)
    {
        if(strcasecmp(value, "yes") == 0)
        {
            synth_channel->synth_voice_params->smi_template_original = 1;
        }
        else if(strcasecmp(value, "no") == 0)
        {
            synth_channel->synth_voice_params->smi_template_original = 0;
        }
    } 
}

/**
 *  Example parser https://github.com/libexpat/libexpat/blob/master/expat/examples/elements.c
 */
static void parser_say_as_node(loadFileCall* arg, xmlNodePtr say_as)
{
    tts_synth_channel_t *synth_channel = arg->channel->method_obj;
    xmlAttrPtr attr;
    for(attr = say_as->properties; NULL != attr; attr = attr->next)
    {
        if(strcasecmp("detail", (char *) attr->name) == 0)
        {
            xmlChar* say_as_string = xmlGetProp(say_as, attr->name);  
            int p = 0;
            int kv = 0;
            int length = xmlStrlen(say_as_string);
            int state = 0;
            char key_name[1025];
            bzero(key_name, 1024);

            char key_value[1025];
            bzero(key_value, 1024);

            for(p = 0; p< length; p++)
            {
                if(say_as_string[p] == '=')
                {
                    state = 1;
                    kv = 0;
                    continue;
                }

                if(say_as_string[p] == ';' || say_as_string[p] == '\0' || say_as_string[p] == '\n')
                {
                    state = 0;
                    kv = 0;

                    parser_add_property(arg, key_name, key_value);

                    bzero(key_value, 1024);
                    bzero(key_name, 1024);

                    continue;
                }

                if(state == 0 && say_as_string[p] == ' ')
                {
                    continue;
                }
                else if(state == 0)
                {
                    key_name[kv] = say_as_string[p];
                    kv++;
                    continue;
                }
                else if(state == 1)
                {
                    key_value[kv] = say_as_string[p];
                    kv++;
                    continue;
                }
            }

            if(state == 1)
            {
                parser_add_property(arg, key_name, key_value);
            }
            break;
        }
    }
}

/**
 *  parse all say-as tags
 */
static void parser_all_say_as_node(loadFileCall* arg, xmlDoc* doc, xmlNodePtr cur)
{ 
    cur = cur->xmlChildrenNode;
    while (cur != NULL)
    { 
        if (xmlStrcmp(cur->name, (const xmlChar *)"say-as") == 0)
        {
            xmlAttrPtr attr;
            for(attr = cur->properties; NULL != attr; attr = attr->next)
            { 
                if(strcasecmp("interpret-as", (char *) attr->name) == 0)
                {
                    xmlChar* say_as_string = xmlGetProp(cur, attr->name);
                    if(xmlStrcasecmp("smi_config", say_as_string) == 0)
                    { 
                        parser_say_as_node(arg, cur);
                    }
                }
            }
        }
        else
        {
            parser_all_say_as_node(arg, doc, cur);
        }
        cur = cur->next;
    }

    return;
}

/**
 *  Example parser https://github.com/libexpat/libexpat/blob/master/expat/examples/elements.c
 */
static void parser_speak_node(loadFileCall* arg, xmlNodePtr speakNode)
{
    tts_synth_channel_t *synth_channel = arg->channel->method_obj;
    /**
     *
     * xml:lang="0"
     * xml:voice="72057594037928106"
     * xml:domain="72057594037927948"
     * xml:accent="default"
     * xml:rate="100"
     * xml:volume="100"
     * xml:pitch="100"
     * xml:style="72057594037927937"
     *
     */
    const char* value;
    const char* key;

    xmlAttrPtr attr;
    for(attr = speakNode->properties; NULL != attr; attr = attr->next)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m add prop to speak node %s=%s\n", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, (char *) attr->name,  xmlGetProp(speakNode, attr->name));

        key = attr->name;
        value = xmlGetProp(speakNode, attr->name);
        parser_add_property(arg, key, value);
    }
}

json_object* get_audio_file_url(loadFileCall* arg, apt_str_t body) {
  
    tts_synth_channel_t *synth_channel = arg->channel->method_obj;
    const char* url_type = mrcp_engine_param_get(arg->channel->engine, "url_type");
    
    if (url_type == NULL){
        sendStopSignal(synth_channel, SYNTHESIZER_COMPLETION_CAUSE_ERROR, "Parametr url_type not set in settings");
        return NULL;
    }

    tts_synth_channel_put_params(arg->request, arg->channel);

    apt_str_t ssml_body;
    apt_string_set(&ssml_body, body.buf);
    
    xmlDoc *doc = xmlReadMemory(body.buf, body.length, NULL, NULL, 0);
    if (doc != NULL)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m xml body=%s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, ssml_body.buf);
        xmlNodePtr root = xmlDocGetRootElement(doc);
        xmlNodePtr metadata = findXMLNodeByName(doc, root, "metadata");
        if(metadata != NULL)
        {
            xmlAttrPtr attr;
            for(attr = metadata->properties; NULL != attr; attr = attr->next)
            {
                apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m add prop to speak node %s=%s\n", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, (char *) attr->name,  xmlGetProp(metadata, attr->name));
                xmlNewProp(root, attr->name, xmlGetProp(metadata, attr->name));
            }

            // remove metadata node from speak
            xmlUnlinkNode(metadata);
             
            parser_all_say_as_node(arg, doc, root);
        
            parser_speak_node(arg, root);

            int size = 0;
            xmlChar *newXMLDoc;
            xmlDocDumpMemory(doc, &newXMLDoc, &size);

            apt_string_assign_n(&ssml_body, newXMLDoc, size, arg->channel->pool);
            
            /*
            * Free associated memory.
            */
            xmlFree(newXMLDoc);
        } 
        else 
        { 
            parser_all_say_as_node(arg, doc, root);
        
            parser_speak_node(arg, root);
            apt_string_assign_n(&ssml_body, body.buf, body.length, arg->channel->pool);
        }
        
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m new xml body=%s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, ssml_body.buf);

        /*
         * Free associated memory.
         */
        xmlFreeDoc(doc);
    }
    else
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m no xml in body=%s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, ssml_body.buf);
    }

    if (
            (synth_channel->synth_voice_params->smi_template_id == 0 && synth_channel->synth_voice_params->smi_template_original != 3)
            ||
            (synth_channel->synth_voice_params->smi_template_id != 0 && synth_channel->synth_voice_params->smi_template_original == 3)
        )
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Parsing error: template_id=%d template_original=%d",
                    synth_channel->channel_id.buf,
                    synth_channel->logging_tag.buf,
                    synth_channel->synth_voice_params->smi_template_id,
                    synth_channel->synth_voice_params->smi_template_original);
        sendStopSignal(synth_channel, SYNTHESIZER_COMPLETION_CAUSE_PARSE_FAILURE, "Parameter smi_template_id or smi_template_original contains errors");
        return NULL;
    }
    // wget -q -O -
    // --post-data="{\"smi_voice_id\":72057594037928106,\"request_id\":1,\"smi_domain_id\":72057594037927948,\"text_ssml\":\"Hello World\",\"url_type\":\"http\",\"file_format\":\"pcmu\"}"
    // http://54.191.105.80:8080/smorphing/1.0/say/file
    // --header="Content-type: application/json"
    // --header="Cookie: XSRF-TOKEN=00000000-0000-0000-0000-000000000000"
    json_object* post_json_object = json_object_new_object();

    if (synth_channel->synth_voice_params->voice_params.voice_id == 0)
    {
        int64_t voice_id = get_smi_voice_id(arg);
        if(!voice_id)
        {
            smi_synth_voice_params *voice_params = synth_channel->synth_voice_params;

            char errorText[600];
            snprintf(errorText, 600, "Voice %s with gender %d and age %d, and language %s or language_id %lld not found",
                        voice_params->voice_params.voice_name.buf,
                        voice_params->header_params->voice_param.gender,
                        (int)voice_params->header_params->voice_param.age,
                        voice_params->language_params.language_name.buf,
                        (long long int)voice_params->language_params.language_id
                    );

            sendStopSignal(synth_channel, SYNTHESIZER_COMPLETION_CAUSE_PARSE_FAILURE, errorText);
            return NULL;
        }

        json_object_object_add(post_json_object, "smi_voice_id", json_object_new_int64(voice_id));
    }
    else
    {
        json_object_object_add(post_json_object, "smi_voice_id", json_object_new_int64(synth_channel->synth_voice_params->voice_params.voice_id));
    }

    json_object_object_add(post_json_object, "request_id", json_object_new_string(synth_channel->request_id));

    json_object_object_add(post_json_object, "smi_domain_id", json_object_new_int64(get_smi_domain_id(arg)));

    json_object_object_add(post_json_object, "text_ssml", json_object_new_string_len(ssml_body.buf, ssml_body.length));

    json_object_object_add(post_json_object, "url_type", json_object_new_string(url_type));

    if(memcmp(arg->descriptor->name.buf, "PCMA", strlen("PCMA")) == 0)
    {
        json_object_object_add(post_json_object, "file_format", json_object_new_string("pcma"));
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m format=PCMA", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
    }
    else
    {
        json_object_object_add(post_json_object, "file_format", json_object_new_string("pcmu"));
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m format=PCMU", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
    }

    if(synth_channel->synth_voice_params->smi_template_id != 0)
    {
        json_object_object_add(post_json_object, "template_id", json_object_new_int64(synth_channel->synth_voice_params->smi_template_id));

        if(synth_channel->synth_voice_params->smi_template_original)
        {
            json_object_object_add(post_json_object, "template_original", json_object_new_string("yes"));
        }
        json_object_object_add(post_json_object, "template_original", json_object_new_string("no"));

        return api_request(arg->channel, post_json_object, "/saytemplate/file");
    }

    return api_request(arg->channel, post_json_object, "/say/file");
}

/***
  * The variable audio_file_path will write the path to the smb file
  * @param audio_file_url the path received from smi api
  * @param data_fs_path path to the mounted smb directory
  * @param audio_file_path the path to the audio file will be recorded here
  * @return -1 if error and 0 if everything is OK
 */
int getSMBaudioFilePath(const char* audio_file_url, const char* data_fs_path, char* audio_file_path) {

    if(data_fs_path == NULL)
    {
        return -1;
    }

    int i;
    for (i = strlen(audio_file_url) - 1; i > 0; i--)
    {
        if (audio_file_url[i] ==  '/')
        {
            apr_snprintf(audio_file_path, 512, "%s%s", data_fs_path, audio_file_url + i);
            return 0;
        }
    }

    return -1;
}

FILE* loadAudioFile(loadFileCall* arg) {

    tts_synth_channel_t *synth_channel = arg->channel->method_obj;
    mTime start;

    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m GlobalCurrentLoadingStep value = %d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->GlobalCurrentLoadingStep);

    if (synth_channel->currentLoadingStep >= MAX_FILES_IN_ANSWER) {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_ERROR, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m loadAudioFile error:  %d (step) >= %d (MAX_FILES_IN_ANSWER)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->GlobalCurrentLoadingStep, MAX_FILES_IN_ANSWER);
        return NULL;
    }
    if (synth_channel->audio_file_url[synth_channel->current_num_audio_file_url].length > 2048) {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_ERROR, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m loadAudioFile error: from url=%s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->audio_file_url[synth_channel->current_num_audio_file_url].buf);
        return NULL;
    }
    const char* data_fs_path = mrcp_engine_param_get(arg->channel->engine, "audio_files_path");
    if(data_fs_path == NULL)
    {
        return NULL;
    }

    char marker_audio_file_url[2048];
    bzero(marker_audio_file_url, 2048);
    strncpy(marker_audio_file_url, synth_channel->audio_file_url[synth_channel->current_num_audio_file_url].buf, synth_channel->audio_file_url[synth_channel->current_num_audio_file_url].length);
    apr_snprintf(marker_audio_file_url + strlen(marker_audio_file_url) - strlen("1.pcmu"), 2048 - strlen(marker_audio_file_url), "%d.mark", synth_channel->currentLoadingStep);

    char audio_file_url[2048];
    bzero(audio_file_url, 2048);
    strncpy(audio_file_url, synth_channel->audio_file_url[synth_channel->current_num_audio_file_url].buf, synth_channel->audio_file_url[synth_channel->current_num_audio_file_url].length);

    if(memcmp(arg->descriptor->name.buf, "PCMA", strlen("PCMA")) == 0)
    {
        apr_snprintf(audio_file_url + strlen(audio_file_url) - strlen("1.pcmu"), 2048 - strlen(audio_file_url), "%d.pcma", synth_channel->currentLoadingStep);
    }
    else
    {
        apr_snprintf(audio_file_url + strlen(audio_file_url) - strlen("1.pcmu"), 2048 - strlen(audio_file_url), "%d.pcmu", synth_channel->currentLoadingStep);
    }

    char audio_file_path[2048];
    bzero(audio_file_path, 2048);

    char marker_audio_file_path[2048];
    bzero(marker_audio_file_path, 2048);

    const char* request_timeout = mrcp_engine_param_get(arg->channel->engine, "request_timeout");
    const char* request_tries = mrcp_engine_param_get(arg->channel->engine, "request_tries");

    if(memcmp(audio_file_url, "smb://", strlen("smb://")) != 0)
    {
        synth_channel->isSMB = FALSE;
        apr_snprintf(marker_audio_file_path, 512, "%s%s_marker_%d", data_fs_path, arg->request->channel_id.session_id.buf, synth_channel->GlobalCurrentLoadingStep);
        apr_snprintf(audio_file_path, 512, "%s%s_%d", data_fs_path, arg->request->channel_id.session_id.buf, synth_channel->GlobalCurrentLoadingStep);

        char load_audion_command[512 + 2048];

        apr_snprintf(load_audion_command, 512 + 2048, "wget  --timeout=%s  --tries=%s --output-document=%s \"%s\"", request_timeout, request_tries, audio_file_path, audio_file_url);
        //apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m loadAudioFile api: %s", synth_channel->channel_id.buf, load_audion_command);
        exec(load_audion_command, synth_channel->channel_id.buf, synth_channel->logging_tag.buf, arg->channel);

        if(synth_channel->is_canceled)
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pthread_exit(0)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
            pthread_exit(0);
        }


        if (synth_channel->files_array[synth_channel->GlobalCurrentLoadingStep] != NULL) {
            fclose(synth_channel->files_array[synth_channel->GlobalCurrentLoadingStep]);
        }

        start = mtime();
        FILE* fd = fopen(audio_file_path, "rb");
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m fopen(%s) time=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, audio_file_path, mtime() - start);

        if (fd == NULL) {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m loadAudioFile step=%d not ready", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->GlobalCurrentLoadingStep);
            return NULL;
        }

        long size;

        fseek(fd, 0, SEEK_END);
        size = ftell(fd);
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m loadAudioFile step=%d size=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->GlobalCurrentLoadingStep, size);
        if (size == 0) {
            fclose(fd);
            remove(audio_file_path);
            return NULL;
        }

        fclose(fd);


        char marker_load_audion_command[512 + 2048];

        apr_snprintf(marker_load_audion_command, 512 + 2048, "wget  --timeout=%s  --tries=%s --output-document=%s \"%s\"", request_timeout, request_tries, marker_audio_file_path, marker_audio_file_url);
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m marker_loadAudioFile api: %s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, marker_load_audion_command);
        exec(marker_load_audion_command, synth_channel->channel_id.buf, synth_channel->logging_tag.buf, arg->channel);

        if(synth_channel->is_canceled)
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pthread_exit(0)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
            pthread_exit(0);
        }


        if (synth_channel->marker_files_array[synth_channel->GlobalCurrentLoadingStep] != NULL) {
            fclose(synth_channel->marker_files_array[synth_channel->GlobalCurrentLoadingStep]);
        }

        mTime start = mtime();
        FILE* marker_fd = fopen(marker_audio_file_path, "rb");
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m fopen(%s) time=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, marker_audio_file_path, mtime() - start);
        if (marker_fd == NULL) {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m marker_loadAudioFile step=%d not ready", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->GlobalCurrentLoadingStep);
            return NULL;
        }

        fclose(marker_fd);
    }
    else
    {
        synth_channel->isSMB = TRUE;
        if(getSMBaudioFilePath(audio_file_url, data_fs_path, audio_file_path) != 0)
        {
            return NULL;
        }

        if(getSMBaudioFilePath(marker_audio_file_url, data_fs_path, marker_audio_file_path) != 0)
        {
            return NULL;
        }
    }

    start = mtime();
    synth_channel->files_array[synth_channel->GlobalCurrentLoadingStep] = fopen(audio_file_path, "rb");
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m fopen(%s) time=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, audio_file_path, mtime() - start);
    if (synth_channel->files_array[synth_channel->GlobalCurrentLoadingStep] == NULL)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m loadAudioFile step=%d error with %s [errno=%d]", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->GlobalCurrentLoadingStep, audio_file_path, errno);
    }

    if (synth_channel->files_array[synth_channel->GlobalCurrentLoadingStep] == NULL) {
        return NULL;
    }

    start = mtime();
    synth_channel->marker_files_array[synth_channel->GlobalCurrentLoadingStep] = fopen(marker_audio_file_path, "rb");
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m fopen(%s) time=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, marker_audio_file_path, mtime() - start);
    if (synth_channel->marker_files_array[synth_channel->GlobalCurrentLoadingStep] == NULL)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m loadAudioFile step=%d error with %s [errno=%d]", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->GlobalCurrentLoadingStep, marker_audio_file_path, errno);
    }


    if (synth_channel->marker_files_array[synth_channel->GlobalCurrentLoadingStep] == NULL) {
        return NULL;
    }



    apt_string_assign(&synth_channel->files_path_array[synth_channel->GlobalCurrentLoadingStep], audio_file_path, arg->channel->pool);
    apt_string_assign(&synth_channel->marker_files_path_array[synth_channel->GlobalCurrentLoadingStep], marker_audio_file_path, arg->channel->pool);

    if(synth_channel->is_canceled)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pthread_exit(0)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        pthread_exit(0);
    }

    char buffer[2048];
    bzero(buffer, 2048);
    smi_marker* newMarker = apr_palloc(arg->channel->pool, sizeof (smi_marker));
    bzero(newMarker, sizeof(smi_marker));

    smi_marker* tmp;
    tmp = newMarker;

    do {
        smi_marker* newMarker2 = apr_palloc(arg->channel->pool, sizeof(smi_marker));
        bzero(newMarker2, sizeof(smi_marker));
        if (fgets(buffer, 2048, synth_channel->marker_files_array[synth_channel->GlobalCurrentLoadingStep]) != NULL)
        {
            bzero(buffer + strlen(buffer) - 2, 2048 - strlen(buffer) + 2);
            apt_string_assign(&newMarker2->name, buffer, arg->channel->pool);
        } else
        {
            break;
        }
        if (fgets(buffer, 2048, synth_channel->marker_files_array[synth_channel->GlobalCurrentLoadingStep]) != NULL)
        {
            newMarker2->position = atoi(buffer);
        } else
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;31msession=%s logging-tag=%s\x1b[0m Parser marker file error, dont have value `%s`", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, buffer);
            break;
        }
        newMarker2->next = NULL;
        tmp->next = newMarker2;

    } while (!feof(synth_channel->marker_files_array[synth_channel->GlobalCurrentLoadingStep]));

    synth_channel->marker_list[synth_channel->GlobalCurrentLoadingStep] = newMarker->next;

    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m loadAudioFile step=%d loaded to %s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->GlobalCurrentLoadingStep,  synth_channel->files_path_array[synth_channel->GlobalCurrentLoadingStep].buf);

    int i = synth_channel->GlobalCurrentLoadingStep;
    synth_channel->GlobalCurrentLoadingStep = i + 1;

    synth_channel->currentLoadingStep = synth_channel->currentLoadingStep + 1;
    return synth_channel->files_array[i];
}

/**
 * Checks if all files are generated (if everything was generated * end.pcmu)
 * @param filename
 * @return FALSE - not everything has already been generated, TRUE - everything has been generated.
 */
apt_bool_t isGenerated(loadFileCall* arg, apt_str_t filename, const char* data_fs_path)
{
    tts_synth_channel_t* synth_channel = arg->channel->method_obj;
    mrcp_engine_t* engine = arg->channel->engine;
    if(memcmp(filename.buf, "smb://", strlen("smb://")) == 0)
    {
        char audio_file_path[512];
        bzero(audio_file_path, 512);

        if(getSMBaudioFilePath(filename.buf, data_fs_path, audio_file_path) != 0)
        {
            return FALSE;
        }

        int len = strlen(audio_file_path);
        strcpy(audio_file_path + len - strlen("1.pcmu") - 1, ".end.pcmu");
        audio_file_path[strlen(audio_file_path) - 1] = filename.buf[strlen(filename.buf) - 1];

        mTime start = mtime();
        FILE* fp = fopen(audio_file_path, "rb");
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m fopen(%s) time=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, audio_file_path, mtime() - start);
        if(fp)
        {
            fclose(fp);
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m isGenerated test file %s = TRUE", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, audio_file_path);
            return TRUE;
        }

        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m isGenerated test file %s = FALSE", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, audio_file_path);
        return FALSE;
    }

    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m isGenerated=%s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, filename.buf);
    char endFileUrl[512];
    bzero(endFileUrl, 512);

    strncpy(endFileUrl, filename.buf, 512 - 1);

    int len = strlen(endFileUrl);
    endFileUrl[len - strlen("1.pcmu")] = 0;

    const char* request_timeout = mrcp_engine_param_get(engine, "request_timeout");
    const char* request_tries = mrcp_engine_param_get(engine, "request_tries");
    char audionURL[600];
    apr_snprintf(audionURL, 600, "wget  --timeout=%s  --tries=%s -q -O - --save-headers --post-data=\"\" --content-on-error \"%send.pcm%c\"", request_timeout, request_tries, endFileUrl, filename.buf[len - 1]);

    char *response_text = exec(audionURL, synth_channel->channel_id.buf, synth_channel->logging_tag.buf, arg->channel);

    if (response_text == NULL || memcmp(response_text, "HTTP/1.1 404", strlen("HTTP/1.1 404")) == 0) {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m isGenerated=%s FALSE (%s)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, filename.buf, audionURL);
        return FALSE;
    }

    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m isGenerated=%s TRUE", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, filename.buf);
    return TRUE;
}


void loadAllFiles(loadFileCall* arg) {
    tts_synth_channel_t *synth_channel = arg->channel->method_obj;

    const char* data_fs_path = mrcp_engine_param_get(arg->channel->engine, "audio_files_path");
    if(data_fs_path == NULL)
    {
        return;
    }

    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pause_on_step_2_loading(%dms)",
            synth_channel->channel_id.buf,
            synth_channel->logging_tag.buf,
            synth_channel->loadingStep1Pause);

    usleep(synth_channel->loadingStep1Pause*1000);
   
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pause_on_step_2_loading(%dms) end",
            synth_channel->channel_id.buf,
            synth_channel->logging_tag.buf,
            synth_channel->loadingStep1Pause);
    while (TRUE)
    {
        if (loadAudioFile(arg) == NULL)
        {
            if(synth_channel->isGenerated)
            {
                return;
            }
            else
            {
                apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m current_num_audio_file_url=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->current_num_audio_file_url);
                synth_channel->isGenerated = isGenerated(arg, synth_channel->audio_file_url[synth_channel->current_num_audio_file_url], data_fs_path);
            }

            if(!synth_channel->isGenerated)
            {
                // sleep(synth_channel->loadingPause);
                
                apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pause on loading error(%dms)",
                        synth_channel->channel_id.buf,
                        synth_channel->logging_tag.buf,
                        synth_channel->pause_on_error);

                usleep(synth_channel->pause_on_error*1000);
                
                apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pause on loading error(%dms) end",
                        synth_channel->channel_id.buf,
                        synth_channel->logging_tag.buf,
                        synth_channel->pause_on_error);

            }
        }
         
        if(synth_channel->is_canceled)
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pthread_exit(0)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
            pthread_exit(0);
        } 
    }
}

/*
 * If you have already finished playback.
 */
static void sendStopSignal(tts_synth_channel_t *synth_channel, mrcp_synth_completion_cause_e mrcp_completion_cause, const char* completion_reason)
{
    if(synth_channel->speak_request == NULL)
    {
        return;
    }

    /* raise SPEAK-COMPLETE event */
    mrcp_message_t *message = mrcp_event_create(synth_channel->speak_request, SYNTHESIZER_SPEAK_COMPLETE, synth_channel->channel->pool);
    if (!message) {
        return;
    }

    /* get/allocate synthesizer header */
    mrcp_synth_header_t *synth_header = mrcp_resource_header_prepare(message);
    if (synth_header) {
        /* set completion cause */
        synth_header->completion_cause = mrcp_completion_cause;
        mrcp_resource_header_property_add(message, SYNTHESIZER_HEADER_COMPLETION_CAUSE);

        char header[600];
        if(synth_channel->last_send_marker.length > 0)
        {
            snprintf(header, 600, "timestamp=%lld;%s", (long long int)time(NULL), synth_channel->last_send_marker.buf);
        }
        else
        {
            snprintf(header, 600, "timestamp=%lld", (long long int)time(NULL));
        }
        apt_string_assign(&synth_header->speech_marker, header, synth_channel->channel->pool);
        mrcp_resource_header_property_add(message, SYNTHESIZER_HEADER_SPEECH_MARKER);

        if(completion_reason != NULL)
        {
            apt_string_assign(&synth_header->completion_reason, completion_reason, synth_channel->channel->pool);
            mrcp_resource_header_property_add(message, SYNTHESIZER_HEADER_COMPLETION_REASON);
        }
    }

    /* set request state */
    message->start_line.request_state = MRCP_REQUEST_STATE_COMPLETE;

    synth_channel->speak_request = NULL;
    synth_channel->is_stoped_audio = TRUE;
    /* send asynch event */
    apt_bool_t res_send = mrcp_engine_channel_message_send(synth_channel->channel, message);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_message_send(%d) sendStopSignal(%s)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, res_send, completion_reason);
}

static void sendSpeechMarker(tts_synth_channel_t *synth_channel, const char* marker)
{
    if(marker == NULL)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m try to sendSpeechMarker empty marker", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        return;
    }

    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m try to sendSpeechMarker %s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, marker);
    /* raise SPEAK-COMPLETE event */
    mrcp_message_t *message = mrcp_event_create(synth_channel->speak_request, SYNTHESIZER_SPEECH_MARKER, synth_channel->channel->pool);
    if (!message)
    {
        return;
    }

    /* get/allocate synthesizer header */
    mrcp_synth_header_t *synth_header = mrcp_resource_header_prepare(message);
    if (synth_header)
    {
        apt_string_assign(&synth_channel->last_send_marker, marker, synth_channel->channel->pool);

        char header[600];
        if(synth_channel->last_send_marker.length)
        {
            snprintf(header, 600, "timestamp=%lld;%s", (long long int)time(NULL), synth_channel->last_send_marker.buf);
        }
        else
        {
            snprintf(header, 600, "timestamp=%lld", (long long int)time(NULL));
        }
        apt_string_assign_n(&synth_header->speech_marker, header, strlen(header), synth_channel->channel->pool);

        mrcp_resource_header_property_add(message, SYNTHESIZER_HEADER_SPEECH_MARKER);
    }

    /* set request state */
    message->start_line.request_state = MRCP_REQUEST_STATE_INPROGRESS;

    /* send asynch event */
    apt_bool_t res_send = mrcp_engine_channel_message_send(synth_channel->channel, message);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_message_send(%d) sendSpeechMarker", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, res_send);
}

/*
 * If not downloaded the file. Or did not dispatch the request.
 */
static void sendSpeakStop(tts_synth_channel_t *synth_channel, mrcp_message_t *request) {

    /* calculate estimated time to complete */
    if (mrcp_generic_header_property_check(request, GENERIC_HEADER_CONTENT_LENGTH) == TRUE)
    {
        mrcp_generic_header_t *generic_header = mrcp_generic_header_get(request);
        if (generic_header) {
            synth_channel->completed = 1;
        }
    }
}

/**
 * @return 0 - file was not uploaded, 1 - file was successfully uploaded
 */
int LoadFirstFile(loadFileCall* arg, apt_str_t body,mTime start)
{
    tts_synth_channel_t *synth_channel = arg->channel->method_obj;
    mrcp_message_t *request = arg->request;
    mrcp_message_t *response = arg->response;

    synth_channel->isGenerated = 0;
    synth_channel->current_num_audio_file_url++;
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m current_num_audio_file_url=%d incr [%s]", synth_channel->channel_id.buf, synth_channel->logging_tag.buf,   synth_channel->current_num_audio_file_url, body.buf);


    const char *company_name = mrcp_engine_param_get(arg->channel->engine, "company_name");
    const char *server_id = mrcp_engine_param_get(arg->channel->engine, "server_id");

    apr_snprintf(synth_channel->request_id, 4096, "%s-%s-%s-%d", company_name, server_id, arg->request->channel_id.session_id.buf, synth_channel->current_num_audio_file_url);

    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m request_id=%s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf,   synth_channel->request_id);

    const char* file_url = NULL;
    json_object* response_json = get_audio_file_url(arg, body);

    if(response_json == NULL)
    {
        sendStopSignal(synth_channel, SYNTHESIZER_COMPLETION_CAUSE_ERROR, "No speech source");
        return 0;
    }

    if(synth_channel->is_canceled)
    {
        json_object_put(response_json);
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pthread_exit(0)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        pthread_exit(0);
    }

    json_object* results = json_object_object_get(response_json, "results");

    if (results == NULL || json_object_get_type(results) != json_type_object)
    {
        json_object_put(response_json);
        apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m No speech source recive, invalid response json", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        sendStopSignal(synth_channel, SYNTHESIZER_COMPLETION_CAUSE_ERROR, "No speech source recive, invalid response json");
        return 0;
    }

    json_object* results_filename = json_object_object_get( results, "filename");
    if (results_filename == NULL || json_object_get_type(results_filename) != json_type_string)
    {
        json_object_put(response_json);
        apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m No speech source recive, invalid response json filename", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        sendStopSignal(synth_channel, SYNTHESIZER_COMPLETION_CAUSE_ERROR, "No speech source recive, invalid response json filename");
        return 0;
    }

    file_url = json_object_get_string(results_filename);
    if (file_url == NULL)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m No speech source recive", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        json_object_put(response_json);
        sendStopSignal(synth_channel, SYNTHESIZER_COMPLETION_CAUSE_ERROR, "No speech source recive");
        return 0;
    }

    apt_string_assign(&synth_channel->audio_file_url[synth_channel->current_num_audio_file_url], file_url, arg->channel->pool);
    json_object_put(response_json);

    synth_channel->currentLoadingStep = 1;

    synth_channel->time_from_api_answer = mtime();

    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m loadAudioFile from channel_speak", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
    FILE* audio_file = loadAudioFile(arg);

    if(synth_channel->is_canceled)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pthread_exit(0)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        pthread_exit(0);
    }


    mTime end = mtime();

    apt_log(SYNTH_LOG_MARK, APT_PRIO_NOTICE, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m \x1b[1;33mAPI ANSWER\x1b[0m time=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, end - start);

    if (audio_file)
    {
        if(synth_channel->GlobalCurrentLoadingStep <= 2)
        {
            synth_channel->audio_file = audio_file;
            synth_channel->next_marker = synth_channel->marker_list[1];
            if (synth_channel->audio_file)
            {
                apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Set as speech source ", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
            }
        }
    }
    else
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m No speech source found", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        sendStopSignal(synth_channel, SYNTHESIZER_COMPLETION_CAUSE_ERROR, "No speech source found");
        return 0;
    }

    return 1;
}


static void *APR_THREAD_FUNC load_file_thread_function(void *data)
{
    //pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL);
    //pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,NULL);

    loadFileCall* arg = (loadFileCall*)data;
    tts_synth_channel_t *synth_channel = arg->channel->method_obj;
    mrcp_message_t *request = arg->request;
    mrcp_message_t *response = arg->response;
    const mpf_codec_descriptor_t *descriptor = arg->descriptor;
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m load_file_thread_function start", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);

    mTime start = mtime();
    synth_channel->currentPlayingStep = 1;
    synth_channel->GlobalCurrentLoadingStep = 1;

    synth_channel->speak_request = request;

    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m payload_type=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, descriptor->payload_type);
    //apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m format=%s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, descriptor->format.buf);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m name=%s", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, descriptor->name.buf);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m sampling_rate=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, descriptor->sampling_rate);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m enabled=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, descriptor->enabled);

    if(synth_channel->is_canceled)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pthread_exit(0)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
        pthread_exit(0);
    }

    if (request->body.buf != NULL)
    {
        char *HeaderValue = GetHeaderValue(request, "Content-Type");
        if(HeaderValue != NULL && strstr(HeaderValue, "multipart") != NULL)
        {
            apt_multipart_content_t *multipart = apt_multipart_content_assign(&arg->request->body, NULL,arg->channel->pool);
            if(multipart != NULL)
            {
                apt_bool_t is_final;
                apt_content_part_t content_part;
                while(apt_multipart_content_get(multipart,&content_part,&is_final) == TRUE)
                {
                    if(synth_channel->is_canceled)
                    {
                        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pthread_exit(0)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
                        pthread_exit(0);
                    }

                    if(is_final == TRUE)
                    {
                        break;
                    }

                    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;31msession=%s\x1b[0m multipart request inside while func", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
                    if (LoadFirstFile(arg, content_part.body, start) == 0)
                    {
                        break;
                    }

                    if(synth_channel->is_canceled)
                    {
                        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pthread_exit(0)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
                        pthread_exit(0);
                    }

                    loadAllFiles(arg);
                }
                synth_channel->is_final = 1;
            }
        }
        else
        {
            if (LoadFirstFile(arg, request->body, start) == 1)
            {
                if(synth_channel->is_canceled)
                {
                    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m pthread_exit(0)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
                    pthread_exit(0);
                }

                loadAllFiles(arg);
            }
            synth_channel->is_final = 1;
        }
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m all file loaded", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);
    }

    pthread_exit(APR_SUCCESS);
    return NULL;
}


/** Process SPEAK request */
static apt_bool_t tts_synth_channel_speak(mrcp_engine_channel_t *channel, mrcp_message_t *request, mrcp_message_t *response) {

    tts_synth_channel_t *synth_channel = channel->method_obj;
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30;47msession=%s logging-tag=%s channel_speak body [length=%d] [buf=%s]\x1b[0m",
            synth_channel->channel_id.buf, synth_channel->logging_tag.buf, request->body.length, request->body.buf);

    char* kill_on_barge_in = GetHeaderValue(request, "Kill-On-Barge-In");
    if (kill_on_barge_in != NULL){
        if (strcasecmp(kill_on_barge_in, "true") == 0)
        {
            synth_channel->synth_voice_params->header_params->kill_on_barge_in = TRUE;
        } else if (strcasecmp(kill_on_barge_in, "false") == 0)
        {
            synth_channel->synth_voice_params->header_params->kill_on_barge_in = FALSE;
        } else {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m kill-on-barge-in receive garbage(%s)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, kill_on_barge_in);
        }
    }
    const mpf_codec_descriptor_t *descriptor = mrcp_engine_source_stream_codec_get(channel);


    /* get/allocate synthesizer header */
    mrcp_synth_header_t *synth_header = mrcp_resource_header_prepare(response);
    if (synth_header)
    {
        char header[600];
        snprintf(header, 600, "timestamp=%lld", (long long int)time(NULL));
        apt_string_assign(&synth_header->speech_marker, header, channel->pool);
        mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_SPEECH_MARKER);
    }

    /* set request state */
    response->start_line.request_state = MRCP_REQUEST_STATE_INPROGRESS;

    if (!descriptor)
    {
        apt_log(SYNTH_LOG_MARK, APT_PRIO_WARNING, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Failed to Get Codec Descriptor " APT_SIDRES_FMT, synth_channel->channel_id.buf, synth_channel->logging_tag.buf, MRCP_MESSAGE_SIDRES(request));
        response->start_line.status_code = MRCP_STATUS_CODE_METHOD_FAILED;
        //sendSpeakStop(synth_channel, request);
        ///sendStopSignal(synth_channel, MRCP_STATUS_CODE_METHOD_FAILED, "Failed to Get Codec Descriptor");

        /* set completion cause */
        synth_header->completion_cause = SYNTHESIZER_COMPLETION_CAUSE_ERROR;
        apt_string_assign(&synth_header->completion_reason, "Failed to Get Codec Descriptor", synth_channel->channel->pool);
        mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_COMPLETION_REASON);
        mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_COMPLETION_CAUSE);

        response->start_line.request_state = MRCP_REQUEST_STATE_COMPLETE;
        return FALSE;
    }

    if (channel->engine)
    {
        if(synth_channel->load_file_thread == 0)
        {
            loadFileCall* arg = apr_palloc(channel->pool, sizeof(loadFileCall));
            arg->channel = channel;
            arg->request = request;
            arg->response = response;
            arg->descriptor = descriptor;

            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;32msession=%s\x1b[0m channel_speak thread start", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);

            pthread_create(&synth_channel->load_file_thread, NULL, load_file_thread_function, (void *)arg);

        }
        else
        {

            apt_log(SYNTH_LOG_MARK, APT_PRIO_ERROR, "\x1b[1;31msession=%s\x1b[0m channel_speak error load_file_thread(%d) != 0", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->load_file_thread);


            /* get/allocate synthesizer header */
            mrcp_synth_header_t *synth_header = mrcp_resource_header_prepare(response);
            if (synth_header)
            {
                char header[600];
                snprintf(header, 600, "timestamp=%lld", (long long int)time(NULL));
                apt_string_assign(&synth_header->speech_marker, header, channel->pool);
                mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_SPEECH_MARKER);
            }

            /* set request state */
            response->start_line.request_state = MRCP_REQUEST_STATE_INPROGRESS;
            response->start_line.status_code = MRCP_STATUS_CODE_METHOD_NOT_ALLOWED;// MRCP_STATUS_CODE_SUCCESS_WITH_IGNORE;

            /* send asynch event */
            apt_bool_t res_send = mrcp_engine_channel_message_send(synth_channel->channel, response);
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_message_send(%d) INPROGRESS", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, res_send);
            //fileJumpTo(synth_channel, 0);
            //sleep(1);


            //synth_channel->completed = 1;
            // synth_channel->is_canceled = 1;
            //sendStopSignal(synth_channel, MRCP_STATUS_CODE_METHOD_NOT_VALID, "Second speak request method not valid in this state");
            //tts_synth_channel_close(channel);
            //apt_bool_t res = tts_synth_msg_signal(DEMO_SYNTH_MSG_CLOSE_CHANNEL, channel, NULL);
            //apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;31msession=%s\x1b[0m tts_synth_msg_signal(%d)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, res);
            return TRUE;
        }
    }

    return FALSE;
}

/** Process STOP request */
static apt_bool_t tts_synth_channel_stop(mrcp_engine_channel_t *channel, mrcp_message_t *request, mrcp_message_t *response)
{
    tts_synth_channel_t *synth_channel = channel->method_obj;

    response->start_line.message_type = MRCP_MESSAGE_TYPE_RESPONSE;
    response->start_line.request_state = MRCP_REQUEST_STATE_INPROGRESS;
    response->start_line.status_code = MRCP_STATUS_CODE_SUCCESS;

    char header[600];
    mrcp_synth_header_t *synth_header = mrcp_resource_header_prepare(response);
    if(synth_channel->last_send_marker.length)
    {
        snprintf(header, 600, "timestamp=%lld;%s", (long long int)time(NULL), synth_channel->last_send_marker.buf);
    }
    else
    {
        snprintf(header, 600, "timestamp=%lld", (long long int)time(NULL));
    }
    apt_string_assign(&synth_header->speech_marker, header, channel->pool);
    mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_SPEECH_MARKER);
    /* store the request, make sure there is no more activity and only then send the response */
    synth_channel->stop_response = response;

    /* send asynchronous response to STOP request */
    apt_bool_t res_send = mrcp_engine_channel_message_send(synth_channel->channel, synth_channel->stop_response);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_message_send(%d) stop", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, res_send);

    synth_channel->speak_request = NULL;
    synth_channel->is_stoped_audio = TRUE;
    synth_channel->next_marker = NULL;

    /*Doesnt work thread close with pointer synth_channel->load_file_thread*/
    synth_channel->is_canceled = TRUE;

    if (synth_channel->request_id[0] != 0)
    {
        json_object* post_json_object = json_object_new_object();
        json_object_object_add(post_json_object, "request_id", json_object_new_string(synth_channel->request_id));

        json_object* res = api_request(channel, post_json_object, "/cancel");

        if (res != NULL)
        {
            json_object_put(res);
        }
        json_object_put(post_json_object);
    }
    return TRUE;
}

/** Process PAUSE request */
static apt_bool_t tts_synth_channel_pause(mrcp_engine_channel_t *channel, mrcp_message_t *request, mrcp_message_t *response)
{
    tts_synth_channel_t *synth_channel = channel->method_obj;

    synth_channel->paused = TRUE;
    /* send asynchronous response */
    apt_bool_t res_send = mrcp_engine_channel_message_send(channel, response);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_message_send(%d) pause", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, res_send);
    return TRUE;
}

/** Process RESUME request */
static apt_bool_t tts_synth_channel_resume(mrcp_engine_channel_t *channel, mrcp_message_t *request, mrcp_message_t *response)
{
    tts_synth_channel_t *synth_channel = channel->method_obj;
    synth_channel->paused = FALSE;
    /* send asynchronous response */
    apt_bool_t res_send = mrcp_engine_channel_message_send(channel, response);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_message_send(%d) resume", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, res_send);
    return TRUE;
}

/** Process SET-PARAMS request
 * https://freeswitch.org/confluence/display/FREESWITCH/mod_unimrcp
 * The SET-PARAMS function in freeswitch in mod_unimrcp https://github.com/jart/freeswitch/blob/master/src/mod/asr_tts/mod_unimrcp/mod_unimrcp.c#L1143
 */
static apt_bool_t tts_synth_channel_set_params(mrcp_engine_channel_t *channel, mrcp_message_t *request, mrcp_message_t *response)
{
    tts_synth_channel_t *synth_channel = channel->method_obj;
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_set_params body [length=%d] [buf=%s]", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, request->body.length, request->body.buf);

    char * log_tag = GetHeaderValue(request, "logging-tag");
    if (log_tag != NULL)
    {
        apt_string_assign(&synth_channel->logging_tag, log_tag, channel->pool);
    }
    tts_synth_channel_set_params_validator(channel, request, response);
    tts_synth_channel_put_params(request, channel);

    /* send asynchronous response */
    apt_bool_t res_send = mrcp_engine_channel_message_send(channel, response);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_message_send(%d) channel_set_params", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, res_send);
    return TRUE;
}

/** Process GET-PARAMS request */
static apt_bool_t tts_synth_channel_get_params(mrcp_engine_channel_t *channel, mrcp_message_t *request, mrcp_message_t *response)
{
    tts_synth_channel_t *synth_channel = channel->method_obj;
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_get_params body [length=%d] [buf=%s]", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, request->body.length, request->body.buf);
    mrcp_synth_header_t *req_synth_header;
    /* get synthesizer header */
    req_synth_header = mrcp_resource_header_get(request);
    if (req_synth_header)
    {
        tts_synth_channel_t *synth_channel = channel->method_obj;

        mrcp_synth_header_t *res_synth_header = mrcp_resource_header_prepare(response);

        /* check voice age header */
        if (mrcp_resource_header_property_check(request, SYNTHESIZER_HEADER_VOICE_AGE) == TRUE) {
            res_synth_header->voice_param.age = synth_channel->synth_voice_params->header_params->voice_param.age;
            mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_VOICE_AGE);
        }

        /* check voice name header */
        if (mrcp_resource_header_property_check(request, SYNTHESIZER_HEADER_VOICE_NAME) == TRUE) {
            apt_string_set(&res_synth_header->voice_param.name, synth_channel->synth_voice_params->header_params->voice_param.name.buf);
            mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_VOICE_NAME);
        }

        if (mrcp_resource_header_property_check(request, SYNTHESIZER_HEADER_VOICE_GENDER) == TRUE) {
            apt_string_set(&res_synth_header->voice_param.name, synth_channel->synth_voice_params->header_params->voice_param.name.buf);
            mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_VOICE_GENDER);
        }

        if (mrcp_resource_header_property_check(request, SYNTHESIZER_HEADER_VOICE_VARIANT) == TRUE) {
            res_synth_header->voice_param.variant = synth_channel->synth_voice_params->header_params->voice_param.variant;
            mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_VOICE_VARIANT);
        }

        if (mrcp_resource_header_property_check(request, SYNTHESIZER_HEADER_SPEECH_LANGUAGE) == TRUE) {
            apt_string_set(&res_synth_header->speech_language, synth_channel->synth_voice_params->language_params.language_name.buf);
            mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_SPEECH_LANGUAGE);
        }

        if (mrcp_resource_header_property_check(request, SYNTHESIZER_HEADER_SPEAKER_PROFILE) == TRUE) {
            apt_string_set(&res_synth_header->speaker_profile, synth_channel->synth_voice_params->header_params->speaker_profile.buf);
            mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_SPEAKER_PROFILE);
        }

        char header[600];
        if(synth_channel->last_send_marker.length)
        {
            snprintf(header, 600, "timestamp=%lld;%s", (long long int)time(NULL), synth_channel->last_send_marker.buf);
        }
        else
        {
            snprintf(header, 600, "timestamp=%lld", (long long int)time(NULL));
        }
        apt_string_assign(&res_synth_header->speech_marker, header, channel->pool);
        mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_SPEECH_MARKER);
    }

    /* send asynchronous response */
    apt_bool_t res_send = mrcp_engine_channel_message_send(channel, response);
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_message_send(%d) channel_get_params", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, res_send);
    return TRUE;
}

static apt_bool_t tts_synth_channel_control(mrcp_engine_channel_t *channel, mrcp_message_t *request, mrcp_message_t *response)
{
    tts_synth_channel_t *synth_channel = channel->method_obj;
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_control body [length=%d] [buf=%s]", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, request->body.length, request->body.buf);

    /**
     *
        numeric-speech-unit   =   "Second"
                              /   "Word"
                              /   "Sentence"
                              /   "Paragraph"
     */

    mrcp_synth_header_t *synth_header = mrcp_resource_header_prepare(response);

    int send_speak_restart = 0;
    mrcp_synth_header_t *req_synth_header = mrcp_resource_header_get(request);
    if (req_synth_header)
    {
        /* check voice age header */
        if (mrcp_resource_header_property_check(request, SYNTHESIZER_HEADER_JUMP_SIZE) == TRUE)
        {
            int jump = 0;

            char * request_value = GetHeaderValue(request, "Jump-Size");
            if(request_value != NULL)
            {
                const char* step_type = NULL;
                const char* tmpp = request_value;
                do{
                    tmpp++;
                    if(*tmpp != ' ' && *(tmpp - 1) == ' ')
                    {
                        step_type = tmpp;
                        break;
                    }
                }while(*tmpp != 0);

                if(step_type == NULL || strcasecmp(step_type, "Second") != 0)
                {
                    // Error
                    response->start_line.status_code = MRCP_STATUS_CODE_UNSUPPORTED_PARAM_VALUE;
                    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "Set jump error 409 [%s]", request_value);

                    apt_string_assign(&synth_header->completion_reason, "Unsupported param value", synth_channel->channel->pool);
                    mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_COMPLETION_REASON);

                    synth_header->speak_restart = FALSE;
                    mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_SPEAK_RESTART);
                    return FALSE;
                }

                if(request_value[0] == '-')
                {
                    jump = - atoi(request_value+1);

                    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "Set jump [-][%d]", jump);

                    int result_step = fileJump(synth_channel, jump);

                    if(synth_channel->audio_position + result_step <= 0)
                    {
                        send_speak_restart = TRUE;
                    }
                }
                else if(request_value[0] == '+')
                {
                    jump = atoi(request_value+1);

                    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "Set jump [+][%d]", jump);
                    int result_step = fileJump(synth_channel, jump);
                    if(synth_channel->audio_position + result_step <= 0)
                    {
                        send_speak_restart = TRUE;
                    }
                }
                else
                {
                    jump =  atoi(request_value);
                    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "Set jump [to][%d]", jump);
                    if(fileJumpTo(synth_channel, jump) <= 0)
                    {
                        send_speak_restart = TRUE;
                    }
                }

            }

        }
    }

    synth_header->speak_restart = send_speak_restart;
    mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_SPEAK_RESTART);

    return FALSE;
}

static apt_bool_t tts_synth_channel_barge_in_occured(mrcp_engine_channel_t *channel, mrcp_message_t *request, mrcp_message_t *response)
{
    tts_synth_channel_t *synth_channel = channel->method_obj;
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_barge_in_occured", synth_channel->channel_id.buf, synth_channel->logging_tag.buf);

    synth_channel->speak_request = request;
    if (synth_channel->synth_voice_params->header_params->kill_on_barge_in)
    {
        sendStopSignal(synth_channel, SYNTHESIZER_COMPLETION_CAUSE_BARGE_IN, "Kill on barge in");
        return TRUE;
    }
    else
    {
        response->start_line.message_type = MRCP_MESSAGE_TYPE_RESPONSE;
        response->start_line.method_id = SYNTHESIZER_BARGE_IN_OCCURRED;
        response->start_line.status_code = MRCP_STATUS_CODE_SUCCESS;
        response->start_line.request_state = MRCP_REQUEST_STATE_INPROGRESS;

        mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_KILL_ON_BARGE_IN);

        char header[600];
        mrcp_synth_header_t *synth_header = mrcp_resource_header_prepare(response);
        if(synth_channel->last_send_marker.length)
        {
            snprintf(header, 600, "timestamp=%lld;%s", (long long int)time(NULL), synth_channel->last_send_marker.buf);
        }
        else
        {
            snprintf(header, 600, "timestamp=%lld", (long long int)time(NULL));
        }
        apt_string_assign(&synth_header->speech_marker, header, channel->pool);
        mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_SPEECH_MARKER);


        return FALSE;
    }
}

static apt_bool_t tts_synth_channel_define_lexicon(mrcp_engine_channel_t *channel, mrcp_message_t *request, mrcp_message_t *response)
{
    tts_synth_channel_t *synth_channel = channel->method_obj;
    response->start_line.method_id = SYNTHESIZER_DEFINE_LEXICON;
    response->start_line.request_state = MRCP_REQUEST_STATE_INPROGRESS;
    response->start_line.message_type = MRCP_MESSAGE_TYPE_RESPONSE;
    mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_LOAD_LEXICON);
    char header[600];
    if(synth_channel->last_send_marker.length)
    {
        snprintf(header, 600, "timestamp=%lld;%s", (long long int)time(NULL), synth_channel->last_send_marker.buf);
    }
    else
    {
        snprintf(header, 600, "timestamp=%lld", (long long int)time(NULL));
    }
    mrcp_synth_header_t *synth_header = mrcp_resource_header_prepare(response);
    apt_string_assign(&synth_header->speech_marker, header, channel->pool);
    mrcp_resource_header_property_add(response, SYNTHESIZER_HEADER_SPEECH_MARKER);

    if ((synth_channel->paused) || (synth_channel->audio_file == NULL))
    {
        response->start_line.request_state = MRCP_REQUEST_STATE_COMPLETE;
        response->start_line.status_code = MRCP_STATUS_CODE_METHOD_NOT_VALID;
        return FALSE;
    }

    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_define_lexicon body [length=%d] [buf=%s]",
                        synth_channel->channel_id.buf, synth_channel->logging_tag.buf, request->body.length, request->body.buf);


    json_object *post_json = json_object_new_object();
    apt_bool_t load_lexicon;
    char * request_value = GetHeaderValue(request, "Load-Lexicon");

    if (strcasecmp(request_value, "false") == 0)
    {
        load_lexicon = FALSE;
    } else if(strcasecmp(request_value, "true") == 0)
    {
        load_lexicon = TRUE;
    } else
    {
       response->start_line.status_code = MRCP_STATUS_CODE_METHOD_FAILED;
       return FALSE;
    }

    synth_channel->synth_voice_params->header_params->load_lexicon = load_lexicon;
    synth_channel->synth_voice_params->header_params->lexicon_search_order.buf = request->body.buf;

    json_object_object_add(post_json, "load_lexicon", json_object_new_boolean(load_lexicon));
    json_object_object_add(post_json, "lexicon", json_object_new_string(request->body.buf));

    if (api_request(channel, post_json, "/definelexicon") == NULL)
    {
        json_object_put(post_json);
        response->start_line.status_code = MRCP_STATUS_CODE_METHOD_FAILED;
        synth_channel->completed = 1;
        sendStopSignal(synth_channel, SYNTHESIZER_COMPLETION_CAUSE_ERROR, "Error with define-lexicon");
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30mError\x1b[0m with define-lexicon");
        return FALSE;
    }

    response->start_line.status_code = MRCP_STATUS_CODE_SUCCESS;
    json_object_put(post_json);
    return FALSE;
}

/** Dispatch MRCP request */
static apt_bool_t tts_synth_channel_request_dispatch( mrcp_engine_channel_t *channel, mrcp_message_t *request)
{
    apt_bool_t processed = FALSE;
    mrcp_message_t *response = mrcp_response_create(request, request->pool);


    tts_synth_channel_t *synth_channel = channel->method_obj;

    if(synth_channel->channel_id.length == 0){
        apt_string_assign(&synth_channel->channel_id, request->channel_id.session_id.buf, channel->pool);
    }

    switch (request->start_line.method_id)
    {
        case SYNTHESIZER_SET_PARAMS:
            processed = tts_synth_channel_set_params(channel, request, response);
            break;
        case SYNTHESIZER_GET_PARAMS:
            processed = tts_synth_channel_get_params(channel, request, response);
            break;
        case SYNTHESIZER_SPEAK:
            processed = tts_synth_channel_speak(channel, request, response);
            break;
        case SYNTHESIZER_STOP:
            processed = tts_synth_channel_stop(channel, request, response);
            break;
        case SYNTHESIZER_PAUSE:
            processed = tts_synth_channel_pause(channel, request, response);
            break;
        case SYNTHESIZER_RESUME:
            processed = tts_synth_channel_resume(channel, request, response);
            break;
        case SYNTHESIZER_BARGE_IN_OCCURRED:
            processed = tts_synth_channel_barge_in_occured(channel, request, response);
            break;
        case SYNTHESIZER_CONTROL:
            processed = tts_synth_channel_control(channel, request, response);
            break;
        case SYNTHESIZER_DEFINE_LEXICON:
            processed = tts_synth_channel_define_lexicon(channel, request, response);
            break;
        default:
            break;
    }

    if (processed == FALSE)
    {
        /* send asynchronous response for not handled request */
        apt_bool_t res_send = mrcp_engine_channel_message_send(channel, response);
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m channel_message_send(%d) dispatch(%d)", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, res_send, request->start_line.method_id);
    }
    return TRUE;
}

/** Callback is called from MPF engine context to destroy any additional data associated with audio stream */
static apt_bool_t tts_synth_stream_destroy(mpf_audio_stream_t *stream)
{
    return TRUE;
}

/** Callback is called from MPF engine context to perform any action before open */
static apt_bool_t tts_synth_stream_open(mpf_audio_stream_t *stream, mpf_codec_t *codec)
{
    return TRUE;
}

/** Callback is called from MPF engine context to perform any action after close */
static apt_bool_t tts_synth_stream_close(mpf_audio_stream_t *stream)
{
    return TRUE;
}

static int fileJump(tts_synth_channel_t *synth_channel, int delta_sec)
{
    int sampleRate = 8000;
    int bitDepth = 8;
    int channelCount = 1;

    int step = sampleRate * (bitDepth / 8) * channelCount * delta_sec;


    synth_channel->delta_sec += step;
    return synth_channel->delta_sec;
}

static int fileJumpTo(tts_synth_channel_t *synth_channel, int jump_to_sec)
{
    int sampleRate = 8000;
    int bitDepth = 8;
    int channelCount = 1;

    int step = sampleRate * (bitDepth / 8) * channelCount * jump_to_sec;


    synth_channel->jump_to_sec = step;
    return step;
}

/** Callback is called from MPF engine context to read/get new frame */
static apt_bool_t tts_synth_stream_read(mpf_audio_stream_t *stream, mpf_frame_t *frame) {

    mTime start;
    tts_synth_channel_t *synth_channel = stream->obj;

    /* check if there is active SPEAK request and it isn't in paused state */
    if (synth_channel->speak_request && synth_channel->paused == FALSE)
    {
        /* normal processing */
        apt_bool_t completed = FALSE;
        if (synth_channel->audio_file && !synth_channel->is_stoped_audio)
        {
            /* read speech from file */
            apr_size_t size = frame->codec_frame.size;


            if(synth_channel->jump_to_sec != 0)
            {
                apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m jump_to_sec=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->jump_to_sec);

                synth_channel->currentPlayingStep = 1;
                synth_channel->audio_file = synth_channel->files_array[synth_channel->currentPlayingStep];
                synth_channel->next_marker = synth_channel->marker_list[synth_channel->currentPlayingStep];

                if (synth_channel->audio_file)
                {
                    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m [2]Set as Speech Source currentPlayingStep=%d",
                            synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->currentPlayingStep);
                    if(fseek( synth_channel->audio_file , 0 , SEEK_SET) != 0)
                    {
                        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m fseek jump error %d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, errno);
                    }

                    synth_channel->delta_sec = synth_channel->jump_to_sec;
                    synth_channel->jump_to_sec = 0;
                    synth_channel->audio_position = 0;
                }
                else
                {
                    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO,
                            "\x1b[1;30msession=%s logging-tag=%s\x1b[0m open audio file error currentPlayingStep=%d, "
                            "currentLoadingStep=%d, isGenerated=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf,
                            synth_channel->currentPlayingStep,
                            synth_channel->GlobalCurrentLoadingStep,
                            synth_channel->isGenerated);
                }
            }


            if(synth_channel->delta_sec != 0)
            {
                apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m delta_sec=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->delta_sec);
            }

            if(synth_channel->delta_sec > 0)
            {
                // Jump forward
                int pos_old = ftell(synth_channel->audio_file);
                if(fseek( synth_channel->audio_file , synth_channel->delta_sec , SEEK_CUR) != 0)
                {
                    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m fseek jump error %d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, errno);
                    fseek(synth_channel->audio_file, 0L, SEEK_END);
                }
                int pos_new = ftell(synth_channel->audio_file);

                int real_jump = 0;
                if(pos_new != pos_old)
                {
                    real_jump = pos_new - pos_old;
                }
                apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m jump %d / %d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->delta_sec, real_jump);
                synth_channel->delta_sec -= real_jump;
                synth_channel->audio_position -= real_jump;
                return TRUE;
            }

            do{
                // Jump backwards
                if(synth_channel->delta_sec < 0)
                {
                    int pos_old = ftell(synth_channel->audio_file);
                    if(pos_old > abs(synth_channel->delta_sec))
                    {
                        // Jump back within one file
                        if(fseek( synth_channel->audio_file , synth_channel->delta_sec , SEEK_CUR) != 0)
                        {
                            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m fseek jump error %d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, errno);
                        }

                        synth_channel->audio_position += synth_channel->delta_sec;
                        synth_channel->delta_sec = 0;

                        int pos_new = ftell(synth_channel->audio_file);

                        synth_channel->next_marker = synth_channel->marker_list[synth_channel->currentPlayingStep];
                        if(synth_channel->next_marker != NULL)
                        {
                            while(synth_channel->next_marker->position < pos_new)
                            {
                                if(synth_channel->next_marker->next == NULL)
                                {
                                    break;
                                }

                                synth_channel->next_marker = synth_channel->next_marker->next;
                            }
                        }
                    }
                    else if(synth_channel->currentPlayingStep > 1)
                    {
                        // Jump back outside the current file
                        synth_channel->delta_sec += pos_old;
                        synth_channel->audio_position += pos_old;

                        synth_channel->currentPlayingStep--;
                        synth_channel->audio_file = synth_channel->files_array[synth_channel->currentPlayingStep];
                        synth_channel->next_marker = synth_channel->marker_list[synth_channel->currentPlayingStep];

                        if (synth_channel->audio_file)
                        {
                            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m [3]Set as Speech Source currentPlayingStep=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->currentPlayingStep);
                            if(fseek( synth_channel->audio_file , 0 , SEEK_END) != 0)
                            {
                                apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m fseek jump error %d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, errno);
                            }
                        }
                        else
                        {
                            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO,
                                    "\x1b[1;30msession=%s logging-tag=%s\x1b[0m open audio file error currentPlayingStep=%d, "
                                    "currentLoadingStep=%d, isGenerated=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf,
                                    synth_channel->currentPlayingStep,
                                    synth_channel->GlobalCurrentLoadingStep,
                                    synth_channel->isGenerated);
                        }
                    }
                    else
                    {
                        // Jump back beyond the beginning of the first file
                        if(fseek( synth_channel->audio_file , 0 , SEEK_SET) != 0)
                        {
                            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m fseek jump error %d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, errno);
                        }
                        synth_channel->delta_sec = 0;
                        synth_channel->audio_position = 0;
                        synth_channel->next_marker = synth_channel->marker_list[1];
                    }
                }
                else
                {
                    break;
                }
            }while(synth_channel->delta_sec != 0);

            if(synth_channel->next_marker != NULL)
            {
                int current_pos = ftell(synth_channel->audio_file);
                if(current_pos > 0 && synth_channel->next_marker->position <= current_pos)
                {
                    sendSpeechMarker(synth_channel, synth_channel->next_marker->name.buf);
                    synth_channel->next_marker = synth_channel->next_marker->next;
                }
            }

            if(!synth_channel->audio_file)
            {
                return TRUE;
            }

            start = mtime();

            int fread_result = fread(frame->codec_frame.buffer, 1, size, synth_channel->audio_file); 
            if(fread_result > 0)
            {
                synth_channel->audio_position += fread_result;
            }
            
            apt_log(SYNTH_LOG_MARK, APT_PRIO_DEBUG, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m fread audio file %d, time=%d, result=%d",
                    synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->currentPlayingStep, mtime() - start, fread_result);
             
            if ( fread_result == size)
            {
                frame->type |= MEDIA_FRAME_TYPE_AUDIO;

                if(!synth_channel->is_show_start_read_log)
                {
                    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m \x1b[1;33mfirst read audio file\x1b[0m %d, time=%d, time_from_start=%d, time_from_api_answer=%d",
                            synth_channel->channel_id.buf
                            , synth_channel->logging_tag.buf
                            , synth_channel->currentPlayingStep
                            , mtime() - start
                            , mtime() - synth_channel->time_from_start
                            , mtime() - synth_channel->time_from_api_answer);
                    synth_channel->is_show_start_read_log = TRUE;
                }

            }
            else if (synth_channel->currentPlayingStep >= synth_channel->GlobalCurrentLoadingStep && synth_channel->isGenerated && synth_channel->is_final)
            {
                // Finished if the current file was the last and the generation of the files was completed
                apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO,
                        "\x1b[1;30msession=%s logging-tag=%s\x1b[0m completed audio file currentPlayingStep=%d, currentLoadingStep=%d, "
                        "isGenerated=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf,
                        synth_channel->currentPlayingStep,
                        synth_channel->GlobalCurrentLoadingStep,
                        synth_channel->isGenerated);
                completed = TRUE;
            }
            else if (synth_channel->currentPlayingStep + 1 >= synth_channel->GlobalCurrentLoadingStep && !synth_channel->isGenerated && !synth_channel->is_final)
            {
                // Pause if the current file was the last one and the file generation was not complete
                apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO,
                        "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Pause if the current file was the last one and the file generation was not complete currentPlayingStep=%d, "
                        "currentLoadingStep=%d, isGenerated=%d", synth_channel->channel_id.buf, synth_channel->logging_tag.buf,
                        synth_channel->currentPlayingStep,
                        synth_channel->GlobalCurrentLoadingStep,
                        synth_channel->isGenerated);
                return TRUE;
            }
            else
            {
                // Opening a second or more file
                synth_channel->currentPlayingStep++;
                synth_channel->audio_file = synth_channel->files_array[synth_channel->currentPlayingStep];
                synth_channel->next_marker = synth_channel->marker_list[synth_channel->currentPlayingStep];

                if (synth_channel->audio_file)
                {
                    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m [1]Set as Speech Source currentPlayingStep=%d"
                            , synth_channel->channel_id.buf
                            , synth_channel->logging_tag.buf
                            , synth_channel->currentPlayingStep);
                    
                    if(fread_result <= 0)
                    {
                        return tts_synth_stream_read(stream, frame);
                    }
                     
                    fread_result = fread(frame->codec_frame.buffer + fread_result, 1, size - fread_result, synth_channel->audio_file); 
                    
                    apt_log(SYNTH_LOG_MARK, APT_PRIO_DEBUG, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m fread audio file %d, time=%d, (add)result=%d",
                            synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->currentPlayingStep, mtime() - start, fread_result);

                    frame->type |= MEDIA_FRAME_TYPE_AUDIO;
                    if(fread_result > 0)
                    {
                        synth_channel->audio_position += fread_result;
                    }
                    else
                    {
                        return tts_synth_stream_read(stream, frame);
                    }
                }
                else
                {
                    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO,
                            "\x1b[1;30msession=%s logging-tag=%s\x1b[0m open audio file error currentPlayingStep=%d, "
                            "currentLoadingStep=%d, isGenerated=%d set speek as completed", synth_channel->channel_id.buf, synth_channel->logging_tag.buf,
                            synth_channel->currentPlayingStep,
                            synth_channel->GlobalCurrentLoadingStep,
                            synth_channel->isGenerated);
                    completed = TRUE;
                }
            }
        }
        else
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_DEBUG, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m empty audio", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->currentPlayingStep);

            /* fill with silence in case no file available */
            if (synth_channel->time_to_complete >= CODEC_FRAME_TIME_BASE)
            {
                memset(frame->codec_frame.buffer, 0, frame->codec_frame.size);
                frame->type |= MEDIA_FRAME_TYPE_AUDIO;
                synth_channel->time_to_complete -= CODEC_FRAME_TIME_BASE;
            }
        }

        if (completed || synth_channel->completed != 0)
        {
            apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;32msession=%s\x1b[0m completed", synth_channel->channel_id.buf, synth_channel->logging_tag.buf, synth_channel->currentPlayingStep);
            sendStopSignal(synth_channel, SYNTHESIZER_COMPLETION_CAUSE_NORMAL, "Speak complete");
        }
    }
    return TRUE;
}

static apt_bool_t tts_synth_msg_signal(tts_synth_msg_type_e type, mrcp_engine_channel_t *channel, mrcp_message_t *request)
{
    apt_bool_t status = FALSE;
    tts_synth_channel_t *tts_channel = channel->method_obj;
    tts_synth_engine_t *tts_engine = tts_channel->tts_engine;
    apt_task_t *task = apt_consumer_task_base_get(tts_engine->task);
    apt_task_msg_t *msg = apt_task_msg_get(task);

    if (msg)
    {
        tts_synth_msg_t *tts_msg;
        msg->type = TASK_MSG_USER;
        tts_msg = (tts_synth_msg_t*) msg->data;

        tts_msg->type = type;
        tts_msg->channel = channel;
        tts_msg->request = request;
        status = apt_task_msg_signal(task, msg);
    }

    return status;
}

static apt_bool_t tts_synth_msg_process(apt_task_t *task, apt_task_msg_t *msg)
{
    tts_synth_msg_t *tts_msg = (tts_synth_msg_t*) msg->data;
    switch (tts_msg->type)
    {
        case DEMO_SYNTH_MSG_OPEN_CHANNEL:
            /* open channel and send asynch response */
            mrcp_engine_channel_open_respond(tts_msg->channel, TRUE);
            break;
        case DEMO_SYNTH_MSG_CLOSE_CHANNEL:
            /* close channel, make sure there is no activity and send asynch response */
            mrcp_engine_channel_close_respond(tts_msg->channel);
            break;
        case DEMO_SYNTH_MSG_REQUEST_PROCESS:
            tts_synth_channel_request_dispatch(tts_msg->channel,
                    tts_msg->request);
            break;
        default:
            break;
    }
    return TRUE;
}
