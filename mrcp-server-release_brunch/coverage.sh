#!/bin/bash
 

cd platforms/rest-tests

# Определение coverage
# find ./ -type f -iname '*.gcno' | grep "main" | sed -e 's/main.cpp/*/g'
files=`find ../ -type f -iname '*.gcno' | grep "main" | sed -e 's/main.c/*/g'`
 

lcov --directory .. --capture --output-file cov.tmp1
gcov -b $files



# Генерация отчёта
mkdir cov

genhtml -o ./cov cov.tmp1




rm -rf ./*.gcov


cat cov/plugins/mrcp-to-rest/src/mrcp_to_rest_engine.c.gcov.html | grep "headerCovTableEntryLo" | sed -r "s/.*>(.*?)<.*/coverage=\1/"