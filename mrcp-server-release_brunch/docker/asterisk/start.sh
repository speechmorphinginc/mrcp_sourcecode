#!/usr/bin/env bash
# Vars
##############################################################################
# SIP params
MRCP_IP=${MRCP_IP:-172.17.0.1}
MRCP_PORT=${MRCP_PORT:-8060}
MRCP_CLIENT_IP=${MRCP_CLIENT_IP:-172.17.0.2}
MRCP_CLIENT_PORT=${MRCP_CLIENT_PORT:-25097}
MRCP_RTPMIN=${MRCP_RTPMIN:-5000}
MRCP_RTPMAX=${MRCP_RTPMAX:-20000}

# Internal vars
CONFIG_DIR="/etc/asterisk/"
SERVER_CONFIG="${CONFIG_DIR}/mrcp.conf"
SERVER_CONFIG_TEMPLATE=${SERVER_CONFIG_TEMPLATE:-"${SERVER_CONFIG}.template"}
CONF_VARS=$(set | grep -v CONF_VARS | awk -F "=" '{print $1}' | grep 'MRCP_')
SERVER_BIN=${SERVER_BIN:-"asterisk -cvvvvv"}


##############################################################################
# Functions
function logging () {
    >&2 echo -e "$1"
}

function replace_from_variable () {
    STREAM=$1; VAR_NAME=$2; VAR_VALUE=$3
    DELIMITER="/"
    if [[ $VAR_VALUE = *"/"* ]]; then
        DELIMITER="#"
    fi
    echo -e "${STREAM}" | sed "s${DELIMITER}__${VAR_NAME}__${DELIMITER}${VAR_VALUE}${DELIMITER}g"
    logging "Replace __${VAR_NAME}__ to '${VAR_VALUE}'"
}

function replace_vars () {
    STREAM=$1; VAR_NAME=$2;
    STREAM="$(replace_from_variable "${STREAM}" ${VAR_NAME} ${!VAR_NAME})"
    echo -e "${STREAM}"
}

function generate_config () {
    # Generates config to config dir
    logging 'Generates config...'
    stats=$(cat <<EOF
##############################################################################
# ip:                      ${MRCP_IP}
# sip port:                ${MRCP_PORT}
# rtp ports:               ${MRCP_RTPMIN}-${MRCP_RTPMAX}
# client ip:               ${MRCP_CLIENT_IP}
# client port:             ${MRCP_CLIENT_PORT}
##############################################################################
# Config dir:              ${CONFIG_DIR}
# Server config:           ${SERVER_CONFIG}
# Server config template:  ${SERVER_CONFIG_TEMPLATE}
# Server bin:              ${SERVER_BIN}
##############################################################################
EOF
    )
    logging "${stats}"
    conf="$(cat ${SERVER_CONFIG_TEMPLATE})"
    for name in $CONF_VARS; do
        conf="$(replace_vars "${conf}" $name)"
    done
    logging "Write to file ${SERVER_CONFIG}."
    [ ! -d $CONFIG_DIR ] && mkdir -p $CONFIG_DIR
    echo "${conf}" > $SERVER_CONFIG
    if [ ! $? -eq 0 ]; then
        exit $?
    fi
    logging "Config generated!"
    logging "${conf}"
}

function start_asterisk () {
    $SERVER_BIN
}

##############################################################################
# Main operations
generate_config
start_asterisk
