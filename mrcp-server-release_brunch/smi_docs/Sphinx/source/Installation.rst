.. MRCP Server Documentation documentation master file, created by
   sphinx-quickstart on Tue Mar 13 11:43:57 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Installation
=====================================================

Step-by-step installation instructions
--------------------------------------
#. Install all prerequisites


   #. Install Docker.

   #. Install Docker-Compose.

      .. sourcecode:: bash

        curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-com
        pose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

        chmod +x /usr/local/bin/docker-compose

#. Clone git repository.


    .. code-block:: bash

      git clone https://gitlab.com/vstconsulting/smi_mrcp.git

#. Connect docker to gitlab account.

    .. code-block:: bash

      docker login registry.gitlab.com

   Enter your login and password from gitlab account.


#. Edit settings

   Configuration of MRCP server store in *.env* file in the directory *smi_mrcp*. Open this file with your favorite editor, might need to show hidden files to make *.env* visible. Must modify *SMI_API_TOKEN* and *SMI_API_URL* with **API_TOKEN** and **API_URL** provided by Speechmorphing. MRCP_EXT_IP must be set to the IP address of server's external interface. Other options recommended to change:

   .. code-block:: ini

      SMI_API_URL=http://your.api.url/smorphing/1.0

      SMI_API_TOKEN=YourAPIToken

      SMI_AUDIO_PATH=/tmp

      MRCP_EXT_IP=<Your host IP>

#. Run docker-compose using following command in *smi_mrcp* directory:

   .. sourcecode:: bash

      docker-compose up -d


Update MRCP-server
--------------------------

To update MRCP-server, use following commands in *smi_mrcp* directory:

.. sourcecode:: bash

   docker-compose stop
   docker-compose pull
   docker-compose up -d

Configuration options description
---------------------------------

To configure MRCP server, open *.env* in *smi_mrcp* directory with a favorite text editor.

*NOTE* All configuration options must be without any *Spaces*, in such format:

.. sourcecode:: ini

   SMI_VOICE_PARAM_AGE=42

Speechmorphing configuration options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   **SMI_API_URL** - URL to send a request. Mandatory parameter.

   **SMI_API_TOKEN** - token for connecting to API. Mandatory parameter.

   **SMI_AUDIO_PATH** - root path for all audio files. Mandatory parameter. Default value: */tmp/*

   * Following options set default values for options to be used when value not provided in Synthesizer Method Headers or within *SSML*:

      **SMI_DOMAIN_ID** - Domain ID. Default value: *72057594037927948*

      **SMI_VOICE_ID** - Voice ID. Default value: *72057594037928106*

      **SMI_VOICE_PARAM_AGE** - Set voice age parameters. Without default value.

      **SMI_VOICE_PARAM_NAME** - Set voice name parameters. Without default value.

      **SMI_VOICE_PARAM_GENDER** - Set voice gender parameters. Without default value.

      **SMI_SPEECH_LANGUAGE** - Set voice language parameter. Default value: *en*


   **SMI_AGES** - Age to Speechmorphing Age Groups mapping. Default value:

   .. code-block:: ini

       age1:0
       age2:2
       age3:5
       age4:11
       age5:18
       age6:55
       age7:66

MRCP configuration options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   **MRCP_IP** - MRCP internal IP. Default value: *0.0.0.0*

   **MRCP_EXT_IP** - MRCP external IP. Mandatory parameter. If not set, the value of **MRCP_IP** used instead.

   **MRCP_RTPMIN** - Minimum RTP port value. Default value: *5000*

   **MRCP_RTPMAX** - Maximum RTP port value. Default value: *20000*

   **MRCP_V2_PORT** - Port to receive MRCP packages. Default value: *1544*

   **MRCP_RTSP_PORT** - RTSP port. Default value: *1554*

   **MRCP_SERVER_ID** - MRCP server id. Default value: *1*

   **MRCP_CODEC** - Primary MRCP audio codec. Default value: *PCMU*

   **COMPANY_NAME** - Company name. Default value: *SMI*

Debug configuration options
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   **LOGDIR** - Path to a directory on a host machine to store MRCP log files *unimrcpserver-xx.log*. The path shouldn't be set to "/tmp/" directory, if set to "/tmp/" log output will go to console instead. Default value "/tmp/".

   **LOGLEVEL** - Level of logging output. Minimum value *0* (only emergency), maximum value *7* (debug). Default value: *7*. **In production environment recommended setting is 0**


Docker-compose commands
---------------------------------------

#. Run all service in terminal and output log to the terminal when he started, can be stopped by pressing *Ctrl+C*. This run method is effective to debug.

   .. sourcecode:: bash

      docker-compose up

#. Run as service in the background

   .. sourcecode:: bash

      docker-compose up -d

   To control docker running as service:

      #. Start or resume stopped service with logging going to a log file. This method is good for regular work of application.

         .. sourcecode:: bash

            docker-compose start

      #. Stop all services, without removing them, you can resume them at any time.

         .. sourcecode:: bash

            docker-compose stop

#. Restart all running and stopped services

   .. sourcecode:: bash

      docker-compose restart

#. Stop all services and remove containers, networks, volumes and images created by *docker-compose up*

   .. sourcecode:: bash

      docker-compose down
