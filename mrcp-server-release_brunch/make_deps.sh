#!/usr/bin/env bash

ARG_ONE=$1
DEFAULT_CODE=0
INSTAL_TYPE=${ARG_ONE:-"deb"}
UNIMRCP_DEPS_ARCH="unimrcp-deps.tar.gz"
UNIMRCP_DEPS_URL="${UNIMRCP_DEPS_URL:-http://www.unimrcp.org/project/component-view/unimrcp-deps-1-5-0-tar-gz/download}"
UNIMRCP_DEPS_URL_DEB="${UNIMRCP_DEPS_URL_DEB:-http://cloud.vst.lan:8080/swift/v1/test/unimrcp-deps_1.5.0-1_amd64.deb}"
UNIMRCP_DEPS_DIR="${UNIMRCP_DEPS_DIR:-unimrcp-deps-1.5.0}"
UNIMRCP_DEPS_DEB="${UNIMRCP_DEPS_DEB:-unimrcp-deps_1.5.0-1_amd64.deb}"


function download () {
    if [ ! -e $UNIMRCP_DEPS_ARCH ]; then
        wget --no-check-certificate $UNIMRCP_DEPS_URL -O $UNIMRCP_DEPS_ARCH;
    else
        echo "Deps src already loaded."
    fi
}

function unarchive () {
    for step in 1 2 3; do
        echo "Step $step"
        tar -xvzf $UNIMRCP_DEPS_ARCH && break || rm $UNIMRCP_DEPS_ARCH && download
    done
    [ ! -d $UNIMRCP_DEPS_DIR ] && exit 1
}

function download_deps () {
    {
        wget --no-check-certificate $UNIMRCP_DEPS_URL_DEB -O dist/$UNIMRCP_DEPS_DEB;
        if [ -s dist/$UNIMRCP_DEPS_DEB ]; then
            echo "dist/$UNIMRCP_DEPS_DEB not empty"
            dpkg -i dist/$UNIMRCP_DEPS_DEB
            exit $DEFAULT_CODE;
        else
            echo "dist/$UNIMRCP_DEPS_DEB is empty"
            download && unarchive
            exit $DEFAULT_CODE;
        fi
    } || {
        if [ ! -d $UNIMRCP_DEPS_DIR ]; then
            download && unarchive
        fi
    }
    return $?
}

function make_deps () {
    echo "Build deps..."
    sed -i -- 's/sudo//g' build-dep-libs.sh
    ./build-dep-libs.sh -s
    [ "${INSTAL_TYPE}" = "deb" ] && checkinstall --addso -y ./build-dep-libs.sh -s
    return $?
}

function install_deps () {
    [ -e $UNIMRCP_DEPS_DEB ] && dpkg -i $UNIMRCP_DEPS_DEB
    return $?
}


function main () {
    [ ! -d dist/ ] && mkdir dist/

    download_deps
    cd $UNIMRCP_DEPS_DIR
    {
        install_deps || make_deps
        cp $UNIMRCP_DEPS_DEB ../dist/
    } || {
        DEFAULT_CODE=1
    }
    cd ..
    exit $DEFAULT_CODE
}

main
