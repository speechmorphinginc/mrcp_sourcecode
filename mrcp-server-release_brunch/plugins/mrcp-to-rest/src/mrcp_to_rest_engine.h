
#include <unistd.h>
#include <strings.h>
#include <sys/time.h>

#include "mrcp_synth_engine.h"
#include "apt_consumer_task.h"
#include "apt_log.h" 
#include <pthread.h>
#include "../dependencies/json-c/json.h"

#include "apr_thread_proc.h"
#include "apr_file_io.h"
#include "apr_thread_mutex.h"
#include "apr_thread_rwlock.h"
#include "apr_thread_cond.h"
#include "apt_multipart_content.h"

#define mTime long long

mTime mtime() {
    struct timeval t;
    gettimeofday(&t, NULL);
    long long mt = (long long) t.tv_sec * 1000 + t.tv_usec / 1000;
    return mt;
}

#define SYNTH_ENGINE_TASK_NAME "MRCP to REST Engine"

/** Declare this macro to set plugin version */
MRCP_PLUGIN_VERSION_DECLARE

/**
 * Declare this macro to use log routine of the server, plugin is loaded from.
 * Enable/add the corresponding entry in logger.xml to set a cutsom log source priority.
 *    <source name="SYNTH-PLUGIN" priority="DEBUG" masking="NONE"/>
 */
MRCP_PLUGIN_LOG_SOURCE_IMPLEMENT(SYNTH_PLUGIN, "SYNTH-PLUGIN")

/** Use custom log source mark */
#define SYNTH_LOG_MARK   APT_LOG_MARK_DECLARE(SYNTH_PLUGIN)

typedef struct tts_synth_engine_t tts_synth_engine_t;
typedef struct tts_synth_channel_t tts_synth_channel_t;
typedef struct tts_synth_msg_t tts_synth_msg_t;
typedef struct smi_audio_params smi_audio_params;
typedef struct smi_language smi_language;
typedef struct smi_synth_voice_params smi_synth_voice_params;
typedef struct smi_marker smi_marker;

/** Declaration of synthesizer engine methods */
static apt_bool_t tts_synth_engine_destroy(mrcp_engine_t *engine);
static apt_bool_t tts_synth_engine_open(mrcp_engine_t *engine);
static apt_bool_t tts_synth_engine_close(mrcp_engine_t *engine);
static mrcp_engine_channel_t* tts_synth_engine_channel_create(mrcp_engine_t *engine, apr_pool_t *pool);

static const struct mrcp_engine_method_vtable_t engine_vtable = {
    tts_synth_engine_destroy, tts_synth_engine_open,
    tts_synth_engine_close, tts_synth_engine_channel_create
};

/** Declaration of synthesizer channel methods */
static apt_bool_t tts_synth_channel_open(mrcp_engine_channel_t *channel);
static apt_bool_t tts_synth_channel_close(mrcp_engine_channel_t *channel);
static apt_bool_t tts_synth_channel_destroy(mrcp_engine_channel_t *channel);
static apt_bool_t tts_synth_channel_request_process(mrcp_engine_channel_t *channel, mrcp_message_t *request);

static const struct mrcp_engine_channel_method_vtable_t channel_vtable = {
    tts_synth_channel_destroy, tts_synth_channel_open,
    tts_synth_channel_close, tts_synth_channel_request_process
};

/** Declaration of synthesizer audio stream methods */
static apt_bool_t tts_synth_stream_close(mpf_audio_stream_t *stream);
static apt_bool_t tts_synth_stream_destroy(mpf_audio_stream_t *stream);
static apt_bool_t tts_synth_stream_open(mpf_audio_stream_t *stream, mpf_codec_t *codec);
static apt_bool_t tts_synth_stream_read(mpf_audio_stream_t *stream, mpf_frame_t *frame);

static apt_bool_t tts_synth_channel_stop(mrcp_engine_channel_t *channel, mrcp_message_t *request, mrcp_message_t *response);
static void sendStopSignal(tts_synth_channel_t *synth_channel, mrcp_synth_completion_cause_e mrcp_completion_cause, const char* completion_reason);
static int fileJump(tts_synth_channel_t *synth_channel, int delta_sec);
static int fileJumpTo(tts_synth_channel_t *synth_channel, int delta_sec);

static const mpf_audio_stream_vtable_t audio_stream_vtable = {
    tts_synth_stream_destroy, tts_synth_stream_open,
    tts_synth_stream_close, tts_synth_stream_read,
    NULL,
    NULL,
    NULL,
    NULL
};

/** Declaration of demo synthesizer engine */
struct tts_synth_engine_t {
    apt_consumer_task_t *task;
};

struct smi_audio_params {
    int volume;
    int rate;
    int pitch;
};

struct smi_language {
    apt_str_t language_name;
    unsigned long long language_id;
    apt_bool_t supported;
    apt_bool_t is_rtl;
};

typedef struct smi_voice {
    unsigned long long voice_id;
    apt_str_t voice_name;
} smi_voice;

struct smi_synth_voice_params{
    mrcp_synth_header_t *header_params;
    smi_audio_params audio_params;
    smi_language language_params;
    smi_voice voice_params;
    unsigned long long smi_domain_id;
    unsigned long long smi_template_id;
    unsigned long long smi_style_id;
    int smi_template_original;
    apt_str_t speach_accent;
};

#define MAX_FILES_IN_ANSWER 2048
#define OPEN_FILE_NUMBER_OF_ATTEMPTS 7
#define OPEN_FILE_ATTEMPT_INTERVAL 1

struct smi_marker{
    smi_marker* next;
    unsigned int position;
    apt_str_t name;
};


typedef struct {
    mrcp_engine_channel_t *channel;
    mrcp_message_t *request;
    mrcp_message_t *response;
    const mpf_codec_descriptor_t *descriptor;
} loadFileCall;


/** Declaration of demo synthesizer channel */
struct tts_synth_channel_t {
    /** Back pointer to engine */
    tts_synth_engine_t *tts_engine;
    /** Engine channel base */
    mrcp_engine_channel_t *channel;

    /** Active (in-progress) speak request */
    mrcp_message_t *speak_request;
    /** Pending stop response */
    mrcp_message_t *stop_response;
    /** Estimated time to complete */
    apr_size_t time_to_complete;

    /**Request ID in format: <server_name_from_config>-<unimrcp_session_id>*/
    char request_id[4096];

    int completed;

    apt_str_t channel_id;
    apt_str_t logging_tag;

    /** Is paused */
    apt_bool_t paused;

    /**
     * All files upload completed
     */
    apt_bool_t is_final;
    
    /**
     * Cancel uploading files
     */
    apt_bool_t is_canceled;
    
    /**
     * Stop playing audio
     */
    apt_bool_t is_stoped_audio;

    /** Speech source (used instead of actual synthesis) */
    FILE *audio_file;
    int delta_sec;
    int jump_to_sec;
    int audio_position;

    apt_bool_t is_show_start_read_log;
    mTime time_from_start;
    mTime time_from_api_answer;

    int pause_on_error;
    int loadingStep1Pause;

    /**
     * Parameters from set-params for voice
     */
    smi_synth_voice_params *synth_voice_params;


    /**
     * 1 - if already all the files were generated (the .end file was not with the status 404)
     */
    apt_bool_t isGenerated;

    /**
     * Download file number (locally for query)
     */
    int currentLoadingStep;

    /**
     * Download file number (globally for multipart request)
     */
    int GlobalCurrentLoadingStep;

    /**
     * File number of the file to play
     */
    int currentPlayingStep;

    /**
     * URL of the first file to be downloaded
     */
    apt_str_t audio_file_url[MAX_FILES_IN_ANSWER];
    int current_num_audio_file_url;

    apt_bool_t isSMB;

    /**
     * Array of open files
     */
    FILE* files_array[MAX_FILES_IN_ANSWER];

    /**
     * Array of downloaded file names
     */
    apt_str_t files_path_array[MAX_FILES_IN_ANSWER];

    /**
     * Array of open files
     */
    FILE* marker_files_array[MAX_FILES_IN_ANSWER];

    /**
     * Array of downloaded file names
     */
    apt_str_t marker_files_path_array[MAX_FILES_IN_ANSWER];

    smi_marker* marker_list[MAX_FILES_IN_ANSWER];
    smi_marker* next_marker;
    apt_str_t last_send_marker;

    pthread_t load_file_thread;
};

typedef enum {
    DEMO_SYNTH_MSG_OPEN_CHANNEL,
    DEMO_SYNTH_MSG_CLOSE_CHANNEL,
    DEMO_SYNTH_MSG_REQUEST_PROCESS
} tts_synth_msg_type_e;

/** Declaration of demo synthesizer task message */
struct tts_synth_msg_t {
    tts_synth_msg_type_e type;
    mrcp_engine_channel_t *channel;
    mrcp_message_t *request;
};

static apt_bool_t tts_synth_msg_signal(tts_synth_msg_type_e type, mrcp_engine_channel_t *channel, mrcp_message_t *request);
static apt_bool_t tts_synth_msg_process(apt_task_t *task, apt_task_msg_t *msg);

/**
 * 
 * @param cmd
 * @param session
 * @param log_tag
 * @param channel
 * @return 
 */
char* exec(const char* cmd, const char* session, const char* log_tag, mrcp_engine_channel_t* channel) {
 
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m CURL API CALL [%s]", session, log_tag, cmd);
    mTime start = mtime();

    static const int buff_size = 1024; // size of one piece of data
    static const int max_buffer_size = 1024*1024*5; //maximum size of output buffer
    int real_buff_size = buff_size;
    // run program
    FILE* fp = popen(cmd, "r");
    if (NULL == fp) {

        mTime end = mtime();
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m CURL API ANSWER NULL time=%d[cmd=%s]", session, log_tag, end - start, cmd); 
        return NULL;
    }

    // read stdout
    char *buff = malloc(buff_size * sizeof (char));
    bzero(buff, buff_size);
    int count = fread(buff, 1, buff_size, fp);
    if (0 == count) { // no stdout, something terrible happened
        free(buff);
        fclose(fp);
        mTime end = mtime();
        apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m CURL API ANSWER 0 time=%d[cmd=%s]", session, log_tag, end - start, cmd); 
        return NULL;
    }

    // read until the end of stdout
    if (0 != buff[buff_size - 1]) 
    {
        // last symbol not zero, buffer filled
        char *sub_buff = NULL;
        int i = 2; // twice as initial buffer, increasing during iterations
        while (0 != count) 
        {
            if ((buff_size * i) > max_buffer_size){
                apt_log(SYNTH_LOG_MARK, APT_PRIO_ERROR, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Want realloc memory more than max_buffer_size=%d, buff_size=%d", session, log_tag, max_buffer_size, buff_size * i); 
            }
            char *new_buff = realloc(buff, buff_size * i * sizeof (char));
            if (new_buff != NULL)
            {
                buff = new_buff;
                real_buff_size = buff_size * i * sizeof (char);
            } 
            else 
            {
                apt_log(SYNTH_LOG_MARK, APT_PRIO_ERROR, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m Cannot realloc memory for output variable, size of buffer=%d", session, log_tag, buff_size * i); 
            }
            
            sub_buff = buff;
            sub_buff += buff_size * (i - 1);
            bzero(sub_buff, buff_size);
            count = fread(sub_buff, 1, buff_size, fp);
            i++;
        }
    }
     
    char *p_buff = apr_palloc(channel->pool, real_buff_size);
    memcpy(p_buff, buff, real_buff_size);
    free(buff);
    fclose(fp);
    mTime end = mtime();
    apt_log(SYNTH_LOG_MARK, APT_PRIO_INFO, "\x1b[1;30msession=%s logging-tag=%s\x1b[0m CURL API ANSWER time=%d buff=%s[cmd=%s]", session, log_tag, end - start, p_buff, cmd); 
    return p_buff;
}

char *addslashes(const char *str) {
    size_t i = 0, x = 0, j = 0, size_x = 0;
    char *str2 = NULL;

    for (i = 0; str[i]; i++) {
        if (str[i] == 0x5c)
            x++;
        else if (str[i] == 0x27)
            x++;
        else if (str[i] == 0x22)
            x++;
    }

    size_x = x + i + 1;

    str2 = malloc(size_x * sizeof (char));

    for (i = 0; str[i]; i++) {
        char c = str[i];

        if (c == 0x27) {
            str2[j] = 0x5c;
            j++;
            str2[j] = c;
        } else if (c == 0x5c) {
            str2[j] = c;
            j++;
            str2[j] = c;
        } else if (c == 0x22) {
            str2[j] = 0x5c;
            j++;
            str2[j] = c;
        } else {
            str2[j] = c;
        }

        j++;
    }

    str2[j] = 0x0;
    return str2;
}
 
/**
 * Executes the request to api
 * @param channel
 * @param json
 * @param url
 * @return Returns NULL if error and json_object * if success
 *
 * @note do not forget that from json_object * you need to free the memory with the json_object_put command;
 */
json_object* api_request(mrcp_engine_channel_t* channel, json_object* json, const char *url) {

    tts_synth_channel_t *synth_channel = channel->method_obj;
    const char* request_timeout = mrcp_engine_param_get(channel->engine, "request_timeout");
    const char* request_tries = mrcp_engine_param_get(channel->engine, "request_tries");

    static const char* wget_cmd_template = "wget -q -O - --post-data=\"%s\" "
            "%s%s --header=\"Content-type: application/json\" --timeout=%s  --tries=%s "
            "--header=\"Cookie: XSRF-TOKEN=%s\" ";
    size_t wct_len = strlen(wget_cmd_template);

    // get api credentials from UniMRCP configuration
    const mrcp_engine_t *engine = channel->engine;
    const char* api_url = mrcp_engine_param_get(engine, "api_url");
    const char* api_token = mrcp_engine_param_get(engine, "api_token");

    if (api_url == NULL || api_token == NULL){
        sendStopSignal(synth_channel, SYNTHESIZER_COMPLETION_CAUSE_ERROR, "Parameter api_url or api_token contains errors");
        return NULL;
    }

    // create json string
    char *post_data = addslashes(json_object_to_json_string(json));

    // prepare wget command line
    size_t cmd_len = wct_len + strlen(post_data) + strlen(api_url) + strlen(url) + strlen(api_token);
    char *cmd = apr_palloc(channel->pool, cmd_len * sizeof(char));
    apr_snprintf(cmd, cmd_len, wget_cmd_template, post_data, api_url, url, request_timeout, request_tries, api_token);

    // execute command
    char* response_text = exec(cmd, synth_channel->channel_id.buf, synth_channel->logging_tag.buf, channel);
 
    if (response_text == NULL || strlen(response_text) == 0){
        free(post_data); 
        return NULL;
    }
 
    json_object* response_json = json_tokener_parse(response_text);
  
    // cleanup
    free(post_data); 
    // return data
    return response_json;
}

/*
 * Function to get value from list header_section.ring
 * @param request - request message with header.header_section
 * @param Name - name of element of header
 */
char* GetHeaderValue(mrcp_message_t* request, char* Name)
{
    apt_header_field_t *list_element;
    list_element = request->header.header_section.ring.prev;
    int step = 0;
    do {
        if (strcasecmp(list_element->name.buf, Name) == 0)
        {
            return list_element->value.buf;
        }
        else
        {

            *list_element = *list_element->link.prev;
        }
        if (list_element->name.length == 0){
            return NULL;
        }
        step++;
    }while(step < 100);

    step = 0;
    list_element = request->header.header_section.ring.prev;
    do {
        if (strcasecmp(list_element->name.buf, Name) == 0)
        {
            return list_element->value.buf;
        }
        else
        {
            *list_element = *list_element->link.next;
        }
        if (list_element->name.length == 0){
            return NULL;
        }
        step++;
    }while(step < 100);
    return NULL;

}
