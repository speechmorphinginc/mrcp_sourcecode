#! /usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import threading
from multiprocessing.dummy import Pool as ThreadPool
from time import sleep
import logging

# TEST SETTINGS
THREADS = 1
CHUNK = 1
FILES = THREADS * 10

TTS_SSML = '''<?xml version="1.0"?>
<speak version="1.1" xmlns="http://www.w3.org/2001/10/synthesis"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.w3.org/2001/10/synthesis
                 http://www.w3.org/TR/speech-synthesis11/synthesis.xsd"
       xml:lang="en-US">
                 
Go from <mark name="here"/> here, to <mark name="there"/> there!
</speak>
'''
# UniMRCP client root directory
err = 0
ROOT_DIR = "../../../../trunk"
# Just detect various directory layout constellations
if not os.path.isdir(ROOT_DIR):
    ROOT_DIR = "../../../../UniMRCP"
if not os.path.isdir(ROOT_DIR):
    ROOT_DIR = "../../../../unimrcp"
if not os.path.isdir(ROOT_DIR):
    ROOT_DIR = "/usr/local/unimrcp"
# UniMRCP profile to use for communication with server
MRCP_PROFILE = "uni1"
# File to write synthesized text to (ROOT_DIR/data/PCM_OUT_FILE)
PCM_OUT_FILE = "UniSynth.pcm"

sys.path.append("./wrapper")
# Import UniMRCP symbols
from UniMRCP import *


# User-defined logger
class UniSynthLogger(UniMRCPLogger):
    def __init__(self):
        super(UniSynthLogger, self).__init__()

    # Log messages go here
    def Log(self, file, line, prio, message):
        # print "  %s\n" % message,  # Ensure atomic logging, do not intermix
        print("{}\n".format(message))
        return True


class UniSynthSession(UniMRCPClientSession):
    def __init__(self, client, profile):
        super(UniSynthSession, self).__init__(client, profile)

    def OnTerminate(self, status):
        print("Session terminated with code {}".format(status))
        return True

    def OnTerminateEvent(self):
        print("Session terminated unexpectedly")
        return True


class UniSynthStream(UniMRCPStreamTx):
    def __init__(self, pcmFile):
        super(UniSynthStream, self).__init__()
        # Set to True upon SPEAK's IN-PROGRESS
        self.started = False
        # Output file descriptor
        self.f = open(pcmFile, 'wb')
        # Buffer to copy audio data to
        self.buf = None

    # Called when an audio frame arrives
    def WriteFrame(self):
        if not self.started:
            return False
        frameSize = self.GetDataSize()
        if frameSize:
            if not self.buf:
                self.buf = bytearray(frameSize)
            self.GetData(self.buf)
            self.f.write(self.buf)
        return True


class UniSynthTermination(UniMRCPAudioTermination):
    def __init__(self, sess, outfile):
        super(UniSynthTermination, self).__init__(sess)
        self.stream = UniSynthStream(outfile)

    def OnStreamOpenTx(self, enabled, payload_type, name, format, channels, freq):
        # Configure outgoing stream here
        return self.stream


class UniSynthChannelBase(UniMRCPSynthesizerChannel):
    fail_codes = {
        MRCP_SIG_STATUS_CODE_SUCCESS: "MRCP_SIG_STATUS_CODE_SUCCESS",
        MRCP_SIG_STATUS_CODE_FAILURE: "MRCP_SIG_STATUS_CODE_FAILURE",
        MRCP_SIG_STATUS_CODE_TERMINATE: "MRCP_SIG_STATUS_CODE_TERMINATE",
        MRCP_SIG_STATUS_CODE_CANCEL: "MRCP_SIG_STATUS_CODE_CANCEL",
    }
    message_types = {
        MRCP_MESSAGE_TYPE_UNKNOWN_TYPE: "MRCP_MESSAGE_TYPE_UNKNOWN_TYPE",
        MRCP_MESSAGE_TYPE_REQUEST: "MRCP_MESSAGE_TYPE_REQUEST",
        MRCP_MESSAGE_TYPE_RESPONSE: "MRCP_MESSAGE_TYPE_RESPONSE",
        MRCP_MESSAGE_TYPE_EVENT: "MRCP_MESSAGE_TYPE_EVENT",
    }

    def __init__(self, sess, term, sem, text, kill_on_barge_in=False):
        super(UniSynthChannelBase, self).__init__(sess, term)
        self.sess = sess
        self.term = term
        self.sem = sem
        self.text = text
        self._kill_on_barge_in = kill_on_barge_in

    # Shorthand for graceful fail: Write message, release semaphore and return False
    def Fail(self, msg, release=True):
        global err
        logging.error(msg)
        err = 1
        if release:
            self.sem.release()
        return False

    # MRCP connection established, start communication
    def OnAdd(self, status):
        if status != MRCP_SIG_STATUS_CODE_SUCCESS:
            return self.Fail("Failed to add channel: CODE: {}".format(
                self.fail_codes.get(status, status)
            ))
        # Start processing here
        msg = self.CreateMessage(SYNTHESIZER_SPEAK)
        msg.content_type = "text/plain"
        msg.SetBody(self.text)
        if self._kill_on_barge_in:
            msg.kill_on_barge_in = True
        return msg.Send()

    # Response or event from the server arrived
    def OnMessageReceiveBase(self, message):
        # Analyze message, update your application state and reply messages here
        if message.GetMsgType() == MRCP_MESSAGE_TYPE_RESPONSE:
            if message.GetStatusCode() != MRCP_STATUS_CODE_SUCCESS:
                return self.Fail(
                    "SPEAK request failed: %d" % message.GetStatusCode())
            if message.GetRequestState() != MRCP_REQUEST_STATE_INPROGRESS:
                return self.Fail("Failed to start SPEAK processing")
            # Start writing audio to the file
            self.term.stream.started = True
            return True  # Does not actually matter
        if message.GetMsgType() != MRCP_MESSAGE_TYPE_EVENT:
            return self.Fail("Unexpected message from the server")
        if message.GetEventID() == SYNTHESIZER_SPEAK_COMPLETE:
            print("Speak complete: {} {}".format(message.completion_cause,
                                                 message.completion_reason))
            self.term.stream.started = False
            self.sem.release()
            return True  # Does not actually matter
        return self.Fail("Unknown message received")

class UniSynthChannelBargeIn(UniSynthChannelBase):

    def OnMessageReceive(self, message):
        if message.GetMsgType() == MRCP_MESSAGE_TYPE_RESPONSE:
            if message.GetStatusCode() != MRCP_STATUS_CODE_SUCCESS:
                return self.Fail(
                    "SPEAK request failed: %d".format(
                        self.fail_codes.get(message.GetStatusCode(),
                                            message.GetStatusCode())
                    ))
            # Start writing audio to the file
        if message.GetRequestState() == MRCP_REQUEST_STATE_INPROGRESS:
            msgGD = self.CreateMessage(SYNTHESIZER_BARGE_IN_OCCURRED)
            self.term.stream.started = True
            return msgGD.Send()
        if message.GetMsgType() != MRCP_MESSAGE_TYPE_EVENT:
            return self.Fail(
                "Unexpected message from the server: \nBody: {}\nType:{}".format(
                    message.GetBody(), message.GetMsgType()
                ),
                True
            )
        if message.GetEventID() == SYNTHESIZER_SPEAK_COMPLETE:
            print("Speak complete: {} {}".format(message.completion_cause, message.completion_reason))
            self.term.stream.started = False
            self.sem.release()
            return True  # Does not actually matter
        elif message.GetEventID() == MRCP_MESSAGE_TYPE_EVENT:
            print("Speak event: {}".format(message))
            return True
        return self.Fail("Unknown message received: {}".format(message.GetMsgType()), True)

class UniSynthChannelMultipart(UniSynthChannelBase):
    def OnAdd(self, status):
        if status != MRCP_SIG_STATUS_CODE_SUCCESS:
            return self.Fail("Failed to add channel: CODE: {}".format(
                self.fail_codes.get(status, status)
            ))
        # Start processing here
        msg = self.CreateMessage(SYNTHESIZER_SPEAK)
        msg.content_type = "multipart/mixed; boundary=\"break\""
        if self._kill_on_barge_in:
            msg.kill_on_barge_in = True
        msg.SetBody("""--break
Content-Type:application/ssml+xml
Content-Length: 658

<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="1" xml:voice="72057594037928106" xml:domain="72057594037927948" xml:accent="default" xml:rate="100" xml:volume="100" xml:pitch="100" xml:style="72057594037927937">1 1 1 1 1 Start first message. ashcan Volkhov Gavialis K Grace. End first message.  If lexicon data is specified by external URI reference, the media
   type 'text/uri-list' (see RFC 2483 [RFC2483] ) is used to list the
   one or more URIs that may be dereferenced to obtain the lexicon data.
   All MRCPv2 servers MUST support the http and http URI access
   mechanisms, and MAY support other mechanisms. END END END</speak>
--break
Content-Type:application/ssml+xml
Content-Length: 300

<?xml version="1.0" encoding="UTF-8" ?><speak version="1.0" xml:lang="1" xml:voice="72057594037928106" xml:domain="72057594037927948" xml:accent="default" xml:rate="100" xml:volume="100" xml:pitch="100" xml:style="72057594037927937">2 2 2 2 This is example of mrcp multipart work END END END</speak>
--break
Content-Type:application/ssml+xml
Content-Length: 355

3 3 3 3 The SPEAK request provides the synthesizer resource with the speech
   text and initiates speech synthesis and streaming.  The SPEAK method
   MAY carry voice and prosody header fields that alter the behavior of
   the voice being synthesized, as well as a typed media message body
   containing the actual marked-up text to be spoken. END END END
--break--
""")
        return msg.Send()

    def OnMessageReceive(self, message):
        if message.GetMsgType() == MRCP_MESSAGE_TYPE_RESPONSE:
            if message.GetRequestState() == MRCP_REQUEST_STATE_COMPLETE:
                print("Multipart test done!")
                self.sem.release()
                return True
        return UniSynthChannelBase.OnMessageReceiveBase(self, message)

class UniSynthChannelDefineLexiconStart(UniSynthChannelBase):
    def OnAdd(self, status):
        if status != MRCP_SIG_STATUS_CODE_SUCCESS:
            return self.Fail("Failed to add channel: CODE: {}".format(
                self.fail_codes.get(status, status)
            ))
        # Start processing here
        msg = self.CreateMessage(SYNTHESIZER_DEFINE_LEXICON)
        msg.content_type = "text/plain"
        msg.load_lexicon = True
        msg.SetBody("Some lexicon")
        return msg.Send()

    def OnMessageReceive(self, message):
        if message.GetMsgType() == MRCP_MESSAGE_TYPE_RESPONSE:
            if message.GetRequestState() == MRCP_REQUEST_STATE_COMPLETE:
                print("Lexicon defined OK!")
                self.sem.release()
                return True
        return UniSynthChannelBase.OnMessageReceive(self, message)

class UniSynthChannelDefineLexiconOnProgress(UniSynthChannelBase):
    lexicon_defined = False

    def OnMessageReceive(self, message):
        if message.GetMsgType() == MRCP_MESSAGE_TYPE_RESPONSE:
            if message.GetRequestState() == MRCP_REQUEST_STATE_INPROGRESS and self.lexicon_defined is False:
                msg = self.CreateMessage(SYNTHESIZER_DEFINE_LEXICON)
                msg.SetBody("Some lexicon")
                self.term.stream.started = True
                self.lexicon_defined = True
                return msg.Send()
            elif self.lexicon_defined:
                # print(dir(message))
                if message.GetStatusCode() != 402:
                    return self.Fail("Invalid state for DF: {}".format(message.GetStatusCode()))
                print("DefineLexicon works fine")
                self.sem.release()
                return True
        return UniSynthChannelBase.OnMessageReceive(self, message)



class UniSynthChannelControl(UniSynthChannelBase):
    move = True
    checked = False

    def OnMessageReceive(self, message):
        print("GOT MESSAGE FROM MRCP SERVER")
        if message.GetMsgType() == MRCP_MESSAGE_TYPE_RESPONSE:
            if message.GetStatusCode() != MRCP_STATUS_CODE_SUCCESS:
                return self.Fail("SPEAK request failed: %d" % message.GetStatusCode())
            if message.GetRequestState() != MRCP_REQUEST_STATE_INPROGRESS:
                if self.checked and not self.move:
                    return self.Fail("Failed to start SPEAK processing")
                if message.GetStatusCode() != 200:
                    return self.Fail("Invalid state for CONTROL: {}".format(message.GetStatusCode()))
                print("CONTROL TEST DONE!")
                self.checked = True
                return True
            if self.move and not self.checked:
                msg = self.CreateMessage(SYNTHESIZER_CONTROL)
                msg.JumpSizeTextSet("-1")
                self.move = False
                return msg.Send()  # Does not actually matter
            # Start writing audio to the file
            self.term.stream.started = True
            return True
            # return True
        if message.GetMsgType() != MRCP_MESSAGE_TYPE_EVENT:
            return self.Fail("Unexpected message from the server")
        if message.GetMsgType() == SYNTHESIZER_SPEECH_MARKER_EVENT:
            print("SPEECH_MARCKER: {}\n{}".format(message.speech_marker, message.GetBody()))
            return True
        if message.GetEventID() == SYNTHESIZER_SPEAK_COMPLETE:
            print("Speak complete: {} {}".format(message.completion_cause, message.completion_reason))
            self.term.stream.started = False
            self.sem.release()
            return True  # Does not actually matter
        return self.Fail("Unknown message received")


class BaseTest(object):
    lines = "=" * 60 + "\n"

    def __init__(self, text=' '.join(sys.argv[1:]).strip(), outfile="/dev/null", profile=MRCP_PROFILE):
        self.text = text
        self.outfile = outfile
        self.profile = MRCP_PROFILE
        self.logger = UniSynthLogger()
        self._static_init()
        self.client = UniMRCPClient(ROOT_DIR, True)
        self.sess = UniSynthSession(self.client, self.profile)
        self.term = UniSynthTermination(self.sess, outfile)
        self.term.AddCapability("PCMU", SAMPLE_RATE_8000)

    def _static_init(self):
        try:
            # Initialize platform first
            UniMRCPClient.StaticInitialize(self.logger, APT_PRIO_INFO)
        except RuntimeError as ex:
            print("Unable to initialize platform: {}".format(ex))
            exit(1)

    def get_channel(self, chan_class=UniSynthChannelBase, kill_on_barge_in=False):
        sem = threading.Semaphore(0)
        return (
            chan_class(self.sess, self.term, sem, self.text, kill_on_barge_in),
            sem
        )

    def __del__(self):
        self.term = None
        self.sess = None
        self.client = None
        UniMRCPClient.StaticDeinitialize()

    def test(self):
        raise NotImplementedError()

    def run(self):
        global err
        try:
            err = self.test()
        except RuntimeError as ex:
            err = 1
            print("An error occured: {}".format(ex))
        except BaseException as ex:
            err = 1
            print("Unknown error occured {}".format(ex))
        finally:
            self.banner("TEST END")
        return err

    def banner(self, msg):
        print("{l}{m}\n{l}".format(l=self.lines, m=msg))

class Test(BaseTest):
    def test(self):
        # Test kill_on_barge_in on BARGE_IN_OCCURRED
        self.banner("Test kill_on_barge_in OFF")
        chan, sem = self.get_channel(chan_class=UniSynthChannelBargeIn, kill_on_barge_in=False)
        sem.acquire()
        self.banner("Test kill_on_barge_in ON")
        chan, sem = self.get_channel(chan_class=UniSynthChannelBargeIn, kill_on_barge_in=True)
        sem.acquire()

        # END
        chan = None
        return 0

class TestLexicon(BaseTest):
    def test(self):
        # Test define lexicon
        self.banner("Test DefineLexicon on start")
        chan, sem = self.get_channel(chan_class=UniSynthChannelDefineLexiconStart)
        sem.acquire()
        self.banner("Test DefineLexicon on progress")
        chan, sem = self.get_channel(chan_class=UniSynthChannelDefineLexiconOnProgress)
        sem.acquire()

        # END
        chan = None
        return 0

class TestControl(BaseTest):
    def test(self):
        # Test CONTROL
        self.banner("Test Control on progress")
        chan, sem = self.get_channel(chan_class=UniSynthChannelControl)
        sem.acquire()

        # END
        chan = None
        return 0

class TestMultipart(BaseTest):
    def test(self):
        #Test multipart
        self.banner("Test multipart in progress")
        chan, sem = self.get_channel(chan_class=UniSynthChannelMultipart)
        sem.acquire()

        #END
        chan = None
        return 0

#print(Test("Some text;"*10).run())
#print(TestLexicon("Some text;"*10).run())
#print(TestControl(TTS_SSML).run())
print(TestMultipart(outfile='/tmp/remote_server_audio.pcmu', text="""--break
   Content-Type:text/uri-list
   Content-Length:...

   http://www.example.com/ASR-Introduction.ssml
   http://www.example.com/ASR-Document-Part1.ssml
   http://www.example.com/ASR-Document-Part2.ssml
   http://www.example.com/ASR-Conclusion.ssml
   
   --break
   Content-Type:application/ssml+xml
   Content-Length:...

   <?xml version="1.0"?>
       <speak version="1.0"
              xmlns="http://www.w3.org/2001/10/synthesis"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="http://www.w3.org/2001/10/synthesis
                   http://www.w3.org/TR/speech-synthesis/synthesis.xsd"
              xml:lang="en-US">
          <p>
            <s>You have 4 new messages.</s>
            <s>The first is from Stephanie Williams
            and arrived at <break/>
            <say-as interpret-as="vxml:time">0342p</say-as>.</s>

            <s>The subject is <prosody
            rate="-20%">ski trip</prosody></s>
          </p>
       </speak>
   --break--""").run())