MRCP-Balancer
=====================================================


Insruction for start
---------------------------------------------------------

#. In directory **kamailio**, config your **MYSQL-variables.env**:

    **MYSQL_DATABASE** - name of database that be used to save data. Default value **kamailio**.

    **MYSQL_ROOT_PASSWORD** - password for root user. Default value **root**.

    **MYSQL_USER** - user to log in. Default value **root**.

    **MYSQL_PASSWORD** - password for log in user. Default value **root**.

    **MYSQL_HOST** - IP mysql database. Default value **127.0.0.1**.

    **LOG_DIR** - directory to mount loging files. Default value **/tmp/**.

#. Use command
    .. sourcecode::bash

        docker-compose up -d --build --abort-on-container-exit

    After execute command all log files would be save in **LOG_DIR** directory.


Configurate balancer:

#. After run docker-compose. Open any *browser*. Insert **127.0.0.1:8080/** to address line.

#. Log in using username and password, that you write to the **MYSQL-variables.env**.

#. Add new address:

    #. **SetId** - ID of group in which would be add this address.

    #. **Destination** - IP address of MRCP-server. Must be in such form *sip:[address]:[port]*. Without default port.

    #.

#. Now you can call to machine with docker IP, with port 5060, balancer use Round-Robin distribution to decide whom to redirect call.
