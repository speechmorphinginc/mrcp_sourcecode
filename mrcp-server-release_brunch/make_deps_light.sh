#!/usr/bin/env bash

ARG_ONE=$1
DEFAULT_CODE=0
INSTAL_TYPE=${ARG_ONE:-"deb"}
UNIMRCP_DEPS_ARCH="unimrcp-deps.tar.gz"
UNIMRCP_DEPS_URL="${UNIMRCP_DEPS_URL:-http://www.unimrcp.org/project/component-view/unimrcp-deps-1-5-0-tar-gz/download}" 
UNIMRCP_DEPS_DIR="${UNIMRCP_DEPS_DIR:-unimrcp-deps-1.5.0}"
UNIMRCP_DEPS_DEB="${UNIMRCP_DEPS_DEB:-unimrcp-deps_1.5.0-1_amd64.deb}"


function download () {
    if [ ! -e $UNIMRCP_DEPS_ARCH ]; then
        wget --no-check-certificate $UNIMRCP_DEPS_URL -O $UNIMRCP_DEPS_ARCH;
    else
        echo "Deps src already loaded."
    fi
}
  
function make_deps () {
    echo "Build deps..."
    sed -i -- 's/sudo//g' build-dep-libs.sh
    ./build-dep-libs.sh -s
    [ "${INSTAL_TYPE}" = "deb" ] && checkinstall --addso -y ./build-dep-libs.sh -s
    return $?
}
 
function install_deps () {
    [ -e $UNIMRCP_DEPS_DEB ] && dpkg -i $UNIMRCP_DEPS_DEB
    return $?
}

#rm -rf $UNIMRCP_DEPS_ARCH
download
tar -xvzf $UNIMRCP_DEPS_ARCH

mkdir dist --parent

cd $UNIMRCP_DEPS_DIR
make_deps
install_deps
cp $UNIMRCP_DEPS_DEB ../dist/ 
