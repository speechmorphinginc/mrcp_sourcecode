
Build and install on Debian 9

Run this commands for build unimrcp on Debian 9

```
apt-get update
apt-get install -y git make cmake g++ gcc gpp libtool m4 automake pkg-config wget curl libxml2 libxml2-dev cppcheck checkinstall
git clone https://git.vstconsulting.net/smi/mrcp-server.git 
cd mrcp-server 
./make_deps.sh 
./bootstrap 
./configure 
./config.status 
make 

checkinstall --addso -y --pkgversion=0.1
mv mrcp_0.1-1_amd64.deb dist/
docker build . -f ./docker/mrcp-test/Dockerfile.prod --build-arg DEBIAN_RELEASE=9 --tag=vst-mrcp-proxy
```
 