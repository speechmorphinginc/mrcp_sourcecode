.. MRCP Server Documentation documentation master file, created by
   sphinx-quickstart on Tue Mar 13 11:43:57 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Troubleshooting
=====================================================

For any problems with the MRCP service, perform following steps:

   #. Check runing docker using command ``docker-compose ps``, if not runing, check log files and restart it.

   #. Monitor log files in the directory set in option **LOGDIR**. If not enough information in logs, increase **LOGLEVEL** and run again.

   #. Check *open* channel. If log file contain line **"Call demo_synth_stream_open"** that line indicates that server has open communication channel with a client.

   #. Check *start* and *end* of the SPEAK message. If log file contain line **"Set as Speech Source"**, it indicate successful *start* of reading requested audio file; next check if log file contains line **"Call completed"**, it indicates the *end* of SPEAK request processing.

      * **NOTE** Message with text "Speech Source" indicates state and the current file to read.

   #. Make call, if no new information appeared in the log after call attempt, check Firewall settings.

   #. Look for *end* call and *destroy* channel messages. If log file contains line **"Call demo_synth_stream_close"**, it indicates the *end* of the call; next check is line **"Call demo_synth_stream_destroy"** that indicates that channel was successfully *destroyed*

If after following these steps, the issue is not solved, send message to support@vstconsulting.net with detailed description of the problem, steps that led to the problem, information on how to replicate the problem, and attach log files.
