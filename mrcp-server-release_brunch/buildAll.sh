#!/usr/bin/env bash

apt-get update
apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"

apt-get update
apt-get install -y git docker-ce make cmake g++ gcc gpp libtool m4 automake pkg-config wget curl libxml2 libxml2-dev cppcheck checkinstall libjson-c-dev

./make_deps_light.sh
  
./bootstrap 
./configure 
./config.status 
make 
make install
checkinstall --addso -y --pkgversion=0.1
mv mrcp-server_0.1-1_amd64.deb  dist/
docker build . -f ./docker/mrcp-test/Dockerfile.prod --build-arg DEBIAN_RELEASE=9 --tag=vst-mrcp-proxy
