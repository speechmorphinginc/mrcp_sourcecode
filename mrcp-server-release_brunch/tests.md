Build freeswitch 1.6 from sources according to the instructions from here https://freeswitch.org/confluence/display/FREESWITCH/Debian+8+Jessie#Debian8Jessie-BuildingFromSource on one server
When you build, you need to include the mod_unimrcp module in the assembly here https://freeswitch.org/confluence/display/FREESWITCH/mod_unimrcp

After assembly in the file /etc/freeswitch/autoload_configs/unimrcp.conf.xml we put this text
```
<configuration name="unimrcp.conf" description="UniMRCP Client">
  <settings>
    <!-- UniMRCP profile to use for TTS -->
    <param name="default-tts-profile" value="unimrcpserver-mrcp2"/>
    <!-- UniMRCP profile to use for ASR -->
    <param name="default-asr-profile" value="unimrcpserver-mrcp2"/>
    <!-- UniMRCP logging level to appear in freeswitch.log.  Options are:
         EMERGENCY|ALERT|CRITICAL|ERROR|WARNING|NOTICE|INFO|DEBUG -->
    <param name="log-level" value="DEBUG"/>
    <!-- Enable events for profile creation, open, and close -->
    <param name="enable-profile-events" value="false"/>

    <param name="max-connection-count" value="4200"/>
    <param name="offer-new-connection" value="1"/>
    <param name="request-timeout" value="300000"/>
  </settings>

  <profiles>
    <X-PRE-PROCESS cmd="include" data="../mrcp_profiles/*.xml"/>
  </profiles>

</configuration>
```

In the folder /etc/freeswitch/mrcp_profiles/leave one file with this text, do not forget to put your address to unimrcpserver
```
<include>
  <!-- UniMRCP Server MRCPv2 -->
  <profile name="aws-mrcp2" version="2">
      <param name="server-ip" value="54.202.238.210"/> <!-- mrcp server address -->
      <param name="server-port" value="8060"/>
      <param name="resource-location" value=""/>
      <param name="client-ip" value="0.0.0.0"/>
      <param name="client-ext-ip" value="109.110.35.166"/> <!-- Your external ip -->
      <param name="client-port" value="5090"/>
      <param name="sip-transport" value="tcp"/>
      <param name="ua-name" value="Freeswitch"/>
      <param name="sdp-origin" value="Freeswitch"/>
      <param name="rtp-ip" value="0.0.0.0"/>
      <param name="rtp-port-min" value="4000"/>
      <param name="rtp-ext-ip" value="109.110.35.166"/> <!-- Your external ip -->
      <param name="rtp-port-max" value="10000"/>
      <param name="codecs" value="PCMU PCMA L16/96/8000"/>
      <param name="speechsynth" value="demosynth"/>
      <param name="ptime" value="20000" />
      <param name="rtcp-tx-interval" value="50000"/>
      <param name="rtcp-rx-resolution" value="60000"/>
      <param name="request-timeout" value="50000" /> 
    </profile>
</include>
```

In file /etc/freeswitch/dialplan/default.xml insert this dialplan for 20 calls unimrcp when will calling extension `7`
``` 
  <extension name="test_wait"> 
        <condition field="destination_number" expression="^7$"> 
                <action application="answer"/>
                <action application="set" data="tts_engine=unimrcp:unimrcpserver-mrcp2"/>
                <action application="set" data="tts_voice=johnsmith"/>
                <action application="speak" data="This is our English text-to-speech. SoOOOOOOOOO LONG!"/>   
                <action application="hangup"/>
        </condition> 
  </extension>
  <extension name="test_wait"> 
        <condition field="destination_number" expression="^8$"> 
                <action application="answer"/>
                <action application="set" data="tts_engine=unimrcp:unimrcpserver-mrcp2"/>
                <action application="set" data="tts_voice=johnsmith"/>
                <action application="speak" data="one one one two three four. five six seven eight nine ten. END. END. END."/>   
                <action application="hangup"/>
        </condition> 
  </extension>
```


# UniMRCP debian 9 build

```
apt-get update
apt-get install -y libexpat1 libexpat1-dev git make cmake g++ gcc gpp libtool m4 automake pkg-config wget curl  update
git clone https://git.vstconsulting.net/smi/mrcp-server.git
cd mrcp-server/
./make_deps.sh 
./bootstrap
./configure
./config.status
make
checkinstall --addso -y --pkgversion=0.1
mv mrcp_0.1-1_amd64.deb dist/ 

docker build . -f ./docker/mrcp-test/Dockerfile.prod --build-arg DEBIAN_RELEASE=9 --tag=vst-mrcp-proxy
docker run -it vst-mrcp-proxy:latest
```

For Red hat (old instruction)
```
git clone https://github.com/unispeech/unimrcp 
sudo yum install git nano make cmake g++ gcc gpp libtool m4 automake pkg-config 
sudo yum install gcc-c++ wget  -y 
sudo yum install https://rpmfind.net/linux/centos/7.4.1708/os/x86_64/Packages/json-c-devel-0.11-4.el7_0.x86_64.rpm
wget https://github.com/unispeech/unimrcp/archive/unimrcp-1.5.0.tar.gz
tar -xvzf unimrcp-1.5.0.tar.gz 
wget http://www.unimrcp.org/project/component-view/unimrcp-deps-1-5-0-tar-gz/download
tar -xvzf download
cd unimrcp-deps-1.5.0/
./build-dep-libs.sh 
cd ../unimrcp-unimrcp-1.5.0/ 
./bootstrap   
./configure  
make
sudo make install
```

Put file /usr/local/unimrcp/conf/unimrcpserver.xml  with same content, but own network configs 
```
<?xml version="1.0" encoding="UTF-8"?>
<!-- UniMRCP server document -->
<unimrcpserver xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
               xsi:noNamespaceSchemaLocation="unimrcpserver.xsd"
               version="1.0">
  <properties>
    <!--
      If the attribute "type" is set to "auto", IP address is determined implicitly by the hostname.
      This is the default setting.
    -->
    <ip type="auto"/>

    <!--
      If the attribute "type" is set to "iface", IP address is determined by the specified name of
      network interface/adapter.
    -->
    <!-- <ip type="iface">eth0</ip>-->

    <!--
      IP address can also be specified explicitly.
    -->
    <ip>0.0.0.0</ip> 
    <ext-ip>54.202.238.210</ext-ip>  <!-- external ip for mrcp --> 
  </properties>

  <components>
    <!-- Factory of MRCP resources -->
    <resource-factory>
      <resource id="speechsynth" enable="true"/>
      <resource id="speechrecog" enable="true"/>
      <resource id="recorder" enable="true"/>
      <resource id="speakverify" enable="true"/>
    </resource-factory>

    <!-- SofiaSIP MRCPv2 signaling agent -->
    <sip-uas id="SIP-Agent-1" type="SofiaSIP">
      <!--
        By default, "ip" and "ext-ip" addresses, set in the properties, are used. These parameters can
        explicitly be specified per "sip-uas" by means of "sip-ip" and "sip-ext-ip" correspondingly.
      -->
      <sip-ip>0.0.0.0</sip-ip>
      <sip-ext-ip>0.0.0.0</sip-ext-ip>
      <sip-port>8060</sip-port>
      <sip-transport>udp,tcp</sip-transport>
      <!-- <force-destination>true</force-destination> -->
      <ua-name>UniMRCP SofiaSIP</ua-name>
      <sdp-origin>UniMRCPServer</sdp-origin>
      <!-- <sip-t1>500</sip-t1> -->
      <!-- <sip-t2>4000</sip-t2> -->
      <!-- <sip-t4>4000</sip-t4> -->
      <!-- <sip-t1x64>32000</sip-t1x64> -->
      <sip-session-expires>600</sip-session-expires>
      <sip-min-session-expires>120</sip-min-session-expires>
      <!-- <sip-message-output>true</sip-message-output> -->
      <!-- <sip-message-dump>sofia-sip-uas.log</sip-message-dump> -->
    </sip-uas>

    <!-- UniRTSP MRCPv1 signaling agent -->
    <rtsp-uas id="RTSP-Agent-1" type="UniRTSP">
      <!--
        By default, "ip" and "ext-ip" addresses, set in the properties, are used. These parameters can
        explicitly be specified per "rtsp-uas" by means of "rtsp-ip" and "rtsp-ext-ip" correspondingly.
      -->
      <!-- <rtsp-ip>10.10.0.1</rtsp-ip> -->
      <!-- <rtsp-ext-ip>a.b.c.d</rtsp-ext-ip> -->
      <rtsp-port>1554</rtsp-port>
      <!-- <force-destination>true</force-destination> -->
      <resource-map>
        <param name="speechsynth" value="speechsynthesizer"/>
        <param name="speechrecog" value="speechrecognizer"/>
      </resource-map>
      <max-connection-count>20000</max-connection-count>
      <inactivity-timeout>600</inactivity-timeout>
      <sdp-origin>UniMRCPServer</sdp-origin>
    </rtsp-uas>

    <!-- MRCPv2 connection agent -->
    <mrcpv2-uas id="MRCPv2-Agent-1">
      <!--
        By default, "ip" address, set in the properties, is used. This parameter can explicitly be
        specified per "mrcpv2-uas" by means of "mrcp-ip".
      -->
      <mrcp-ip>0.0.0.0</mrcp-ip>
      <mrcp-ext-ip>0.0.0.0</mrcp-ext-ip>
      <mrcp-port>1544</mrcp-port>
      <max-connection-count>20000</max-connection-count>
      <max-shared-use-count>20000</max-shared-use-count>
      <force-new-connection>false</force-new-connection>
      <rx-buffer-size>2048</rx-buffer-size>
      <tx-buffer-size>2048</tx-buffer-size>
      <inactivity-timeout>600</inactivity-timeout>
      <termination-timeout>3</termination-timeout>
    </mrcpv2-uas>

    <!-- Media processing engine -->
    <media-engine id="Media-Engine-1">
      <realtime-rate>1</realtime-rate>
    </media-engine>

    <!-- Factory of RTP terminations -->
    <rtp-factory id="RTP-Factory-1">
      <!--
        By default, "ip" and "ext-ip" addresses, set in the properties, are used. These parameters can
        explicitly be specified per "rtp-factory" by means of "rtp-ip" and "rtp-ext-ip" correspondingly.
      -->
      <!-- <rtp-ip>0.0.0.0</rtp-ip> --> 
      <rtp-ext-ip>54.202.238.210</rtp-ext-ip> <!-- external ip for mrcp --> 
      <rtp-port-min>5000</rtp-port-min>
      <rtp-port-max>20000</rtp-port-max>
    </rtp-factory>

    <!-- Factory of plugins (MRCP engines) -->
    <plugin-factory> 
      <engine id="MRCP-to-REST-1" name="mrcptorest" enable="true">
          
        <max-channel-count>100</max-channel-count>
        <param name="api_url" value="http://54.191.105.80:8080/smorphing/1.0"/> 
        <param name="api_token" value="00000000-0000-0000-0000-000000000000"/>
        <param name="url_type" value="smb"/>
        <param name="audio_files_path" value=""/>  
        
        <param name="smi_domain_id" value="72057594037927948"/>
        <param name="smi_voice_id" value="72057594037928106"/>
        
        <!-- AGES TABLE -->
        <param name="age1" value="0"/>
        <param name="age2" value="2"/>
        <param name="age3" value="5"/>
        <param name="age4" value="11"/>
        <param name="age5" value="18"/>
        <param name="age6" value="56"/>
        <param name="age7" value="66"/>
        
        <!-- SYNTHESIZER_HEADER_VOICE_AGE -->
        <param name="voice_param_age" value=""/>
        
        <!-- SYNTHESIZER_HEADER_VOICE_NAME -->
        <param name="voice_param_name"/>
        
        <!-- SYNTHESIZER_HEADER_VOICE_GENDER -->
        <param name="voice_param_gender" value=""/>
        
        <!-- SYNTHESIZER_HEADER_VOICE_VARIANT -->
        <param name="voice_param_variant" value=""/>
        
        <!-- SYNTHESIZER_HEADER_SPEECH_LANGUAGE -->
        <param name="speech_language" value="en"/>
        
        <!-- SYNTHESIZER_HEADER_SPEAKER_PROFILE 
        <param name="speaker_profile" value=""/>-->
      </engine>
      <!--
        Engines may have additional named ("max-channel-count") and generic (name/value) parameters.
        For example:
      -->
      <!--
      <engine id="Your-Engine-1" name="yourengine" enable="false">
        <max-channel-count>100</max-channel-count>
        <param name="..." value="..."/>
      </engine>
      --> 
    </plugin-factory>
  </components>

  <settings>
    <!-- RTP/RTCP settings -->
    <rtp-settings id="RTP-Settings-1">
      <jitter-buffer>
        <adaptive>1</adaptive>
        <playout-delay>50</playout-delay>
        <max-playout-delay>600</max-playout-delay>
        <time-skew-detection>1</time-skew-detection>
      </jitter-buffer>
      <ptime>20</ptime>
      <codecs own-preference="false">PCMU PCMA L16/96/8000 PCMU/97/16000 PCMA/98/16000 L16/99/16000</codecs> 
      <!-- Enable/disable RTCP support -->
      <rtcp enable="false">
        <!--
          RTCP BYE policies (RTCP must be enabled first)
            0 - disable RTCP BYE
            1 - send RTCP BYE at the end of session
            2 - send RTCP BYE also at the end of each talkspurt (input)
        -->
        <rtcp-bye>1</rtcp-bye>
        <!-- RTCP transmission interval in msec (set 0 to disable) -->
        <tx-interval>50000</tx-interval>
        <!-- Period (timeout) to check for new RTCP messages in msec (set 0 to disable) -->
        <rx-resolution>20000</rx-resolution>
      </rtcp>
    </rtp-settings>
  </settings>

  <profiles>
    <!-- MRCPv2 default profile -->
    <mrcpv2-profile id="uni2">
      <sip-uas>SIP-Agent-1</sip-uas>
      <mrcpv2-uas>MRCPv2-Agent-1</mrcpv2-uas>
      <media-engine>Media-Engine-1</media-engine>
      <rtp-factory>RTP-Factory-1</rtp-factory>
      <rtp-settings>RTP-Settings-1</rtp-settings>

      <!--
        Profile-based association of engines to resources. For example:
      -->
      <!--
      <resource-engine-map>
        <param name="speechsynth" value="Flite-1"/>
        <param name="speechrecog" value="PocketSphinx-1"/>
      </resource-engine-map>
      -->
    </mrcpv2-profile>

    <!-- MRCPv1 default profile -->
    <mrcpv1-profile id="uni1">
      <rtsp-uas>RTSP-Agent-1</rtsp-uas>
      <media-engine>Media-Engine-1</media-engine>
      <rtp-factory>RTP-Factory-1</rtp-factory>
      <rtp-settings>RTP-Settings-1</rtp-settings>
    </mrcpv1-profile>

    <!-- More profiles may follow. -->
  </profiles>
</unimrcpserver>
```

Replace the files "/usr/local/unimrcp/data/demo-8kHz.pcm" and "/usr/local/unimrcp/data/demo-16kHz.pcm" with other files in the .pcm format but with a sound of at least 3 seconds

Install https://github.com/mojolingo/sippy_cup on the third and fourth servers and create a file my_test_scenario.yml with text:
```
---
source: 10.0.0.32
advertise_address: 10.0.0.32
destination: 10.10.10.18
from_user: 1001
to: 7
max_concurrent: 300
calls_per_second: 10
number_of_calls: 300
steps:
  - register
  - invite
  - wait_for_answer
  - ack_answer
  - sleep 60
  - hangup
```
Thus, the call to number 7 will last 3 * 20 seconds and, under the scenario, two simultaneously launched tests will go to 600 simultaneous calls in 30 seconds from the start.


Execute the command to run the test
sudo /usr/local/bin/sippy_cup -r my_test_scenario.yml
 
