apt-get update && apt-get install -y wget
echo "deb http://files.freeswitch.org/repo/deb/freeswitch-1.6/ jessie main" \
	 > /etc/apt/sources.list.d/freeswitch.list
wget -O - --no-check-certificate https://files.freeswitch.org/repo/deb/debian/freeswitch_archive_g0.pub | \
	apt-key add -
apt-get update && apt-get install -y freeswitch-meta-all \
	&& apt-get install -y git

cd /usr/src/ \
	&& git clone --depth=1 https://freeswitch.org/stash/scm/fs/freeswitch.git -bv1.6 freeswitch \
	&& cd freeswitch
apt-get install -y --force-yes freeswitch-video-deps-most autoconf
cd /usr/src/freeswitch/ \
	&& ./bootstrap.sh -j  \
	&& sed -i 's!#asr_tts/mod_unimrcp!asr_tts/mod_unimrcp!' ./modules.conf \
	&& ./configure \
	&& make \
	&& make install
	
apt-get update && apt-get install -y nano mc telnet

apt-get update && apt-get clean \
	&& rm -rf /var/lib/apt/lists/*



chmod +x /usr/local/freeswitch/entrypoint
chmod +x /usr/local/freeswitch