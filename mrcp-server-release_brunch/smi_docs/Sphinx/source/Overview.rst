

Overview
=====================================================

MRCP Server Documentation covers prerequisites, install guide and attempts answer to questions about common user's problems.

MRCP adaptor is designed to be an interface between external system like an IVR and Speech Morphing's synthesizer by convert MRCPv2 requests and responses to HTTP requests managed by Speech Morphing's API. MRCP adaptor receive SIP and MRCP messages, process and convert them to HTTP requests, and streams content of audio files provided as part of API responses.

MRCP adaptor follows RFC6787 including multipart requests, for more information on multipart request see this_

.. _this: https://tools.ietf.org/html/rfc6787
