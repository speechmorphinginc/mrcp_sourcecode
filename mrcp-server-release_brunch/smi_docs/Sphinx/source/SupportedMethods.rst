Supported Methods
=====================================================

MRCP Adaptor supports *RFC 6787 - Media Resource Control Protocol Version 2* as it defined for Speech Synthesizer Resource with the following caveats:

Synthesizer Methods
-------------------

.. tabularcolumns::
      |p{0.23\linewidth}|>{\centering}p{0.15\linewidth}|p{0.57\linewidth}|

+--------------------+---------+--------------------------------------------------------------------------------------+
|METHOD              |SUPPORTED|                          NOTES                                                       |
+====================+=========+======================================================================================+
|**SPEAK**           |    Y    |Synthesizer Header Fields:                                                            | 
|                    |         |                                                                                      |
|                    |         | - **Kill-On-Barge-in**: supported                                                    |
|                    |         | - **Jump-Size**: not supported                                                       |
|                    |         | - **Speaker-Profile**: not supported                                                 |
|                    |         | - **Voice-Parameter**: supported                                                     |
|                    |         |                                                                                      |
|                    |         |   - **voice_name**:                                                                  |
|                    |         |                                                                                      |
|                    |         |    - value is used to search by name via *Speechmorphing API*                        |
|                    |         |    - numeric value mapped to *smi_voice_id* directly                                 |
|                    |         |                                                                                      |
|                    |         |   - **voice-age**: value mapped to *age_id* based on configuration mapping           |
|                    |         |   - **voice-gender**: supported                                                      |
|                    |         |   - **voice-variant**: not supported                                                 |
|                    |         |                                                                                      |
|                    |         | - **Prosody-Parameters**: not supported. Should be defined within SSML               |
|                    |         | - **Speech-Language**: supported. Should be defined within SSML                      |
|                    |         |                                                                                      |
|                    |         |    - value is used to search by name via *Speechmorphing API*                        |
|                    |         |    - numeric value mapped to *language_id* directly                                  |
|                    |         |                                                                                      |
|                    |         | - **Audio-Fetch-Hint**: not supported                                                |
|                    |         | - **Speak-Length**: not supported                                                    |
|                    |         | - **multi-part messages**: supported. *Content-type*:                                |
|                    |         |                                                                                      |
|                    |         |    - *TEXT*: supported                                                               |
|                    |         |    - *SSML*: supported                                                               |
|                    |         |    - *TTS*: supported                                                                |
|                    |         |    - *URI*: not supported                                                            |
|                    |         |    - *URI-LIST*: not supported                                                       |
|                    |         |                                                                                      |
+--------------------+---------+--------------------------------------------------------------------------------------+
|**STOP**            |    Y    |                                                                                      |
+--------------------+---------+--------------------------------------------------------------------------------------+
|**BARGE-IN-OCCURED**|    Y    |                                                                                      |
+--------------------+---------+--------------------------------------------------------------------------------------+
|**PAUSE**           |    Y    |                                                                                      |
+--------------------+---------+--------------------------------------------------------------------------------------+
|**RESUME**          |    Y    |                                                                                      |
+--------------------+---------+--------------------------------------------------------------------------------------+
|**CONTROL**         |Partial  |Synthesizer Header Fields:                                                            |
|                    |         |                                                                                      |
|                    |         | - **Jump-Size**: partially supported, only in seconds                                |
|                    |         | - **Voice-Parameter**: not supported                                                 |
|                    |         |                                                                                      |
|                    |         | - **Prosody-Parameters**: not supported. Should be defined within SSML               |
|                    |         | - **Speak-Restart**: supported                                                       |
|                    |         | - **Speak-Length**: not supported                                                    |
|                    |         |                                                                                      |
+--------------------+---------+--------------------------------------------------------------------------------------+
|**DEFINE-LEXICON**  |    Y    |                                                                                      |
+--------------------+---------+--------------------------------------------------------------------------------------+
|**GET-PARAMS**      |    Y    |                                                                                      |
+--------------------+---------+--------------------------------------------------------------------------------------+
|**SET-PARAMS**      |    Y    |                                                                                      |
+--------------------+---------+--------------------------------------------------------------------------------------+

Synthesizer Events
------------------

.. tabularcolumns::
      |p{0.23\linewidth}|>{\centering}p{0.15\linewidth}|p{0.57\linewidth}|
     
+--------------------+---------+--------------------------------------------------------------------------------------+
|EVENT               |SUPPORTED|                          NOTES                                                       |
+====================+=========+======================================================================================+
|**SPEAK-COMPLETE**  |    Y    |                                                                                      |
|                    |         | - **Completion-Cause**: supported                                                    |
|                    |         | - **Completion-Reason**: supported                                                   |
+--------------------+---------+--------------------------------------------------------------------------------------+
|**SPEECH-MARKER**   |    Y    |                                                                                      |
+--------------------+---------+--------------------------------------------------------------------------------------+

Parameters Use Priority
-----------------------
In order to accomodate multiple variations of SPEAK requests (different Content-types, Speechmorphing-aware or not systems, etc.) 
MRCP Adaptor implemented following priority of parameters mapping:

- **smi_voice_id**:

  #. *xml:voice* provided inside of SSML document
  #. *voice-name* provided in SPEAK request Header Field
  #. *voice-name* provided in SET-PARAMS request Header Field
  #. *SMI_VOICE_ID* configuration option

- **language_id**:

  #. *xml:lang* provided inside of SSML document
  #. *speech-language* provided in SPEAK request Header Field
  #. *speech-language* provided in SET-PARAMS request Header Field
  #. *SMI_SPEECH_LANGUAGE* configuration option

- **smi_domain_id**:

  #. *xml:domain* provided inside of SSML document
  #. *SMI_DOMAIN_ID* configuration option

- **gender_id**:

  #. *voice-gender* provided in SPEAK request Header Field
  #. *voice-gender* provided in SET-PARAMS request Header Field
  #. *SMI_VOICE_PARAM_GENDER* configuration option

- **age_id**:

  #. *voice-age* provided in SPEAK request Header Field
  #. *voice-age* provided in SET-PARAMS request Header Field
  #. *SMI_VOICE_PARAM_AGE* configuration option

- **template_id**:

  #. *xml:template* provided inside of SSML document

- **template_original**:

  #. *xml:template_original* provided inside of SSML document

